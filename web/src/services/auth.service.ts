import BaseService from "./BaseService";
import config from "../config";
import store from "../store";
import { setToken, getToken, removeToken } from "../helpers/auth.helper";
import { history } from "../routes/Routes";
import { User, RegisterUser } from "../types/User";

class AuthService extends BaseService<User>{
  constructor() {
    super('', false);
  }

  async login(email: string, password: string): Promise<any> {
    try {
      const { data: { token, user } } = await this.axios.post('/login', { email, password });
      setToken(token);
      return user;
    } catch (error) {
      throw error;
    }
  }

  async register(user: RegisterUser): Promise<any> {
    try {
      return await this.axios.post('/register', user);
    } catch (error) {
      throw error;
    }
  }

  async confirm(hash: string): Promise<any> {
    try {
      const { data: { token, user } } = await this.axios.post('/confirm', { hash });
      setToken(token);
      return user;
    } catch (error) {
      throw error;
    }
  }

  async changePasswordRequest(email: string): Promise<any> {
    try {
      await this.axios.post('/changePasswordRequest', { email });
    } catch (error) {
      throw error;
    }
  }

  async changePassword(hash: string, senha: string, confirmacaoSenha: string): Promise<any> {
    try {
      await this.axios.post('/changePassword', { hash, senha, confirmacaoSenha });
    } catch (error) {
      throw error;
    }
  }

  async logout() {
    try {
      await this.axios.post('/logout', {},
        {
          headers: { authorization: getToken() },
          baseURL: config.PROTECTED_API_URL
        });
      removeToken();
      store.dispatch({ type: 'CLEAR_USER' });
      history.push('/login');
    } catch (error) {
      this.handleError(error)
    }
  }

  async getUser(): Promise<any> {
    try {
      const result = await this.axios.get('/getUser', { headers: { authorization: getToken() }, baseURL: config.PROTECTED_API_URL });
      return result.data;
    } catch (error) {
      console.log(error);
      this.handleError(error);
    }
  }

  isAuthenticated() {
    return !!getToken();
  }
}

export default new AuthService();