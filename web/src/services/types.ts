export interface GetOptions {
  $status?: string,
  $limit?: number,
  $skip?: number,
  $includedIds?: Array<string> | string,
  $sort?: string,
  $include?: string,
  [key: string]: any
}