import config from '../config';
import axios, { AxiosInstance, AxiosRequestConfig } from 'axios';
import { authInterceptor } from './authInterceptor';
import { GetOptions } from './types';

/**Classe de serviço genérica. */
export default class BaseService<T>{
  constructor(private path: string, isProtected: boolean = true) {
    this.axiosConfig = {
      ...axios.defaults,
      timeout: 5000,
      baseURL: isProtected ? config.PROTECTED_API_URL : config.OPEN_API_URL
    };

    this.axios = axios.create(this.axiosConfig);

    if (isProtected) {
      this.axios.interceptors.request.use(authInterceptor);
    }
  }

  protected axiosConfig: AxiosRequestConfig;
  protected axios: AxiosInstance;

  async getById(id: string): Promise<T> {
    try {
      const result = await this.axios.get(`${this.path}/id/${id}`);
      return result.data;
    } catch (error) {
      return this.handleError(error)
    }
  }

  async restore(id: number) {
    try {
      const result = await this.axios.post(`${this.path}/restore/${id}`);
      return result.data;
    } catch (error) {
      return this.handleError(error);
    }
  }

  async post(path: string, params: T): Promise<T> {
    try {
      const result = await this.axios.post(`${this.path}/${path}`, params);
      return result.data;
    } catch (error) {
      return this.handleError(error)
    }
  }

  async put(path: string, params: T): Promise<T> {
    try {
      const result = await this.axios.put(`${this.path}/${path}`, params);
      return result.data;
    } catch (error) {
      return this.handleError(error)
    }
  }

  async get(path: string, options?: GetOptions) {
    try {
      const result = await this.axios.get(`${this.path}/${path}`, { params: options });
      return result.data;
    } catch (error) {
      return this.handleError(error)
    }
  }

  async delete(id: number) {
    try {
      const result = await this.axios.delete(`${this.path}/${id}`);
      return result.data;
    } catch (error) {
      return this.handleError(error);
    }
  }

  handleError(error: any): any {
    console.error('SERVICE ERROR: ', error);
    switch (error.status) {
      case 0: throw new Error('Falha na comunicação com o servidor.');
      case 401: throw new Error('Ação não autorizada.');
      default: throw new Error('Erro desconhecido.')
    }
  }
}