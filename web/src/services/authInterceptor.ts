import { getToken } from "../helpers/auth.helper";

/**Interceptador de requisições autorizadas. */
export function authInterceptor(config: any): any {
  config.headers['authorization'] = getToken();
  return config;
}