const STORAGE_KEY = 'token';

export function removeToken() {
  localStorage.removeItem(STORAGE_KEY);
}

export function setToken(token: string) {
  localStorage.setItem(STORAGE_KEY, token);
}

export function getToken() {
  return localStorage.getItem(STORAGE_KEY)
}