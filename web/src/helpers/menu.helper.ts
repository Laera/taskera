import menuDefinition, { MenuItem } from "../config/menu";

export function getMenuItemByPath(path: string, items = menuDefinition): MenuItem {
  return items.find(item => {
    if (item.subItems) {
      const result = getMenuItemByPath(path, item.subItems);
      return !!result;
    } else {
      if (path.startsWith(item.path)) {
        return true;
      }
    }
  })
}

export function getHierarchyMenuListByPathRecursively(path: string, item = getMenuItemByPath(path), list: MenuItem[] = []): MenuItem[] {
  if (item) {
    list.push(item);

    if (item.subItems) {
      const subItem = item.subItems.find(item => path.startsWith(item.path));
      getHierarchyMenuListByPathRecursively(path, subItem, list);
    }

    return list;
  }
}
