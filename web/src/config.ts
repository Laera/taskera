const API_URL = process.env.NODE_ENV === 'production' ?
  'www.laerasoftware.com' :
  'localhost';

const API_PORT = 3000;

export default {
  API_PORT,
  API_URL,
  OPEN_API_URL: `http://${API_URL}:${API_PORT}/open`,
  PROTECTED_API_URL: `http://${API_URL}:${API_PORT}/protected`
}