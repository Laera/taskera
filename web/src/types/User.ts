export interface User {
  id: string,
  nome: string,
  email: string,
  caminhoImagem: string,
  sexo: string,
  dataNascimento: Date
}

export interface RegisterUser{
  nome: string,
  email: string,
  senha: string,
  confirmacaoSenha: string,
}