export enum SystemActionTypes {
  LOAD_USER = 'LOAD_USER',
  LOGIN_REQUEST = 'LOGIN_REQUEST',
  LOGIN_SUCCESS = 'LOGIN_SUCCESS',
  LOGIN_ERROR = 'LOGIN_ERROR',
  CONFIRM_REQUEST = 'CONFIRM_REQUEST',
  CONFIRM_SUCCESS = 'CONFIRM_SUCCESS',
  CONFIRM_ERROR = 'CONFIRM_ERROR',
  UNBLOCK_REQUEST = 'UNBLOCK_REQUEST',
  UNBLOCK_SUCCESS = 'UNBLOCK_SUCCESS',
  UNBLOCK_ERROR = 'UNBLOCK_ERROR',
  LOGOUT = 'LOGOUT',
  BLOCK = 'BLOCK',
  UNBLOCK = 'UNBLOCK',
}

export interface SystemState {
  user?: User,
  block?: boolean,
  loading?: boolean,
  error?: any
}

export interface SystemAction {
  type: SystemActionTypes,
  error?: any,
  user?: User
}

export interface User {
  id: number,
  nome: string,
  email: string,
  caminhoImagem: string
}
