import { SystemState, SystemAction, SystemActionTypes } from "./types";

const initalState: SystemState = {}

export default function systemReducer(
  state: SystemState = initalState,
  action: SystemAction): SystemState {
  switch (action.type) {
    case SystemActionTypes.CONFIRM_REQUEST:
    case SystemActionTypes.LOGIN_REQUEST:
    case SystemActionTypes.UNBLOCK_REQUEST:
      return { loading: true }
    case SystemActionTypes.CONFIRM_SUCCESS:
    case SystemActionTypes.LOGIN_SUCCESS:
    case SystemActionTypes.UNBLOCK_SUCCESS:
      return { user: action.user, loading: false, block: false }
    case SystemActionTypes.CONFIRM_ERROR:
    case SystemActionTypes.LOGIN_ERROR:
    case SystemActionTypes.UNBLOCK_ERROR:
      return { error: action.error, loading: false }
    case SystemActionTypes.LOAD_USER:
      return { error: action.error, user: action.user }
    case SystemActionTypes.BLOCK:
      return { ...state, block: true }
    case SystemActionTypes.UNBLOCK:
      return { ...state, block: false }
    case SystemActionTypes.LOGOUT:
      return { ...state, user: undefined }
    default:
      return state;
  }
}