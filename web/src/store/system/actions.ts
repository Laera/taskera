import authService from "../../services/auth.service";
import { SystemAction, User, SystemActionTypes } from "./types";
import { ThunkAction } from "redux-thunk";
import { AppState } from "..";
import { Action } from "redux";
import { toast } from "react-toastify";
import { history } from "../../routes/Routes";
import { removeToken } from "../../helpers/auth.helper";

export function load(): ThunkAction<void, AppState, null, SystemAction> {
  return async dispatch => {
    try {
      const user = await authService.getUser();
      dispatch({ type: SystemActionTypes.LOAD_USER, user });
    } catch (error) {
      removeToken();
      history.replace('/login');
      toast.error('Erro ao carregar usuário.');
    }
  }
}

export function login(email: string, password: string): ThunkAction<any, AppState, null, SystemAction> {
  return async dispattch => {
    try {
      dispattch(request());
      const user = await authService.login(email, password);
      dispattch(success(user));
      history.replace('/home');
      toast.success('Seja bem vindo!');
    } catch (error) {
      dispattch(failure(error));
    }
  }
  function request() { return { type: SystemActionTypes.LOGIN_REQUEST } };
  function success(user: User) { return { type: SystemActionTypes.LOGIN_SUCCESS, user } };
  function failure(error: any) { return { type: SystemActionTypes.LOGIN_ERROR, error } };
}

export function unblock(email: string, password: string, redirect: string = '/home'): ThunkAction<any, AppState, null, SystemAction> {
  return async dispattch => {
    try {
      dispattch(request());
      const user = await authService.login(email, password);
      dispattch(success(user));
      history.replace(redirect);
      toast.success('Seja bem vindo de volta!');
    } catch (error) {
      dispattch(failure(error));
    }
  }
  function request() { return { type: SystemActionTypes.UNBLOCK_REQUEST } };
  function success(user: User) { return { type: SystemActionTypes.UNBLOCK_SUCCESS, user } };
  function failure(error: any) { return { type: SystemActionTypes.UNBLOCK_ERROR, error } };
}

export function confirm(hash: string): ThunkAction<any, AppState, null, SystemAction> {
  return async dispattch => {
    try {
      dispattch(request());
      const user = await authService.confirm(hash);
      dispattch(success(user));
      history.replace('/home');
      toast.success('Cadastro confirmado! Seja bem vindo!');
    } catch (error) {
      dispattch(failure(error));
    }
  }
  function request() { return { type: SystemActionTypes.CONFIRM_REQUEST } };
  function success(user: User) { return { type: SystemActionTypes.CONFIRM_SUCCESS, user } };
  function failure(error: any) { return { type: SystemActionTypes.CONFIRM_ERROR, error } };
}

export function logout(): Action {
  removeToken();
  return { type: SystemActionTypes.LOGOUT };
}

export function block(): Action {
  removeToken();
  return { type: SystemActionTypes.BLOCK }
}
