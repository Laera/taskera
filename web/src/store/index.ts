import systemReducer from './system/reducer';
import thunkMiddleware from 'redux-thunk';
import { createStore, combineReducers, applyMiddleware } from 'redux';

const reducer = combineReducers({
  system: systemReducer
})

const store = createStore(reducer, applyMiddleware(thunkMiddleware));

export default store;
export type AppState = ReturnType<typeof reducer>;