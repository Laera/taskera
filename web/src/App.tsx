import React from 'react';
import Routes, { history } from './routes/Routes';
import store from './store';
import $ from 'jquery';
import { ToastContainer, Flip } from 'react-toastify';
import { Provider } from 'react-redux';
import { load } from './store/system/actions';
//REACT TOASTFY.
import 'react-toastify/dist/ReactToastify.css';
//TEMPLATE.
import 'bootstrap/dist/css/bootstrap.min.css';
import 'owl.carousel/dist/assets/owl.carousel.min.css';
import 'font-awesome/css/font-awesome.min.css';
import './strdash/css/themify-icons.css';
import './strdash/css/slicknav.min.css';
import './strdash/css/typography.css';
import './strdash/css/default-css.css';
import './strdash/css/styles.css';
import './strdash/css/responsive.css';
//PRIMERACT.
import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import './App.css';
//SCRIPTS.
(window as any).jQuery = $;
(window as any).$ = $;
(global as any).jQuery = $;
require('bootstrap');
require('owl.carousel/dist/owl.carousel');
require('jquery-slimscroll');
require('slicknav/jquery.slicknav');

const App: React.FC = () => {


  return (
    <Provider store={store}>
      <ToastContainer newestOnTop={true} position="top-right" transition={Flip} />
      <Routes />
    </Provider>
  );
}

export default App;
