import React from 'react'
import { PropagateLoader } from 'react-spinners'

const css = `
font-size: 20px;
top: calc(50% - 10px);
left: calc(50% - 12.5px);
`
export default function Loading() {
  return (
    <PropagateLoader color="#4A90E2" size={15} sizeUnit="px" css={css} ></PropagateLoader>
  )
}
