import React, { ReactElement } from 'react'
import { PropagateLoader } from 'react-spinners';
import './FullLoading.css';

const css = `
font-size: 20px;
position: absolute;
top: calc(50% - 10px);
left: calc(50% - 10px);
`

function FullLoading(props: any): ReactElement {
  return (
    <div className="overlay">
      <PropagateLoader color="#4A90E2" size={25} sizeUnit="px" css={css} ></PropagateLoader>
    </div>
  )
}

export default FullLoading
