import React, { ReactElement, useLayoutEffect } from 'react'
import SideBar from './sidebar/Sidebar'
import Header from './header/Header'
import Footer from './Footer'
import SubHeader from './header/SubHeader';
import SidebarRight from './sidebar/SidebarRight';
import initialize from '../../strdash/js/layout';
import { connect } from 'react-redux';
import { AppState } from '../../store';
import { logout, block } from '../../store/system/actions';
import { AuthenticatedSessionControl, LogoutTypes } from 'react-session-control';
import { history } from '../../routes/Routes';
import 'metismenujs/dist/metismenujs.css';

interface Props {
  children?: ReactElement,
  logout: typeof logout,
  block: typeof block
}

function Layout({ children, logout, block }: Props): ReactElement {
  useLayoutEffect(() => {
    initialize();
  }, [])

  function handleSessionControlLogout(logoutType: LogoutTypes, local: boolean) {
    if (logoutType === LogoutTypes.inactivity) {
      block();
      history.replace('/lock', { backTo: location.pathname })
    } else {
      logout();
    }
  }

  return (
    <>
      <AuthenticatedSessionControl
        inactivityTimeout={30}
        modalInactivityTimeout={10}
        onLogout={handleSessionControlLogout}
        storageTokenKey="token"
        title="Alerta de inatividade"
        message="Você está muito tempo sem usar o sistema. Deseja sair ou continuar?"
        documentTitleAlertText="ALERTA DE INATIVIDADE"
        timerMessage="Você será deslogado automaticamente em: "
        logoutButtonText="Sair"
        continueButtonText="Continuar"
        debug={false}
      />
      <div className="page-container">
        {/* sidebar menu area start */}
        <SideBar />
        {/* sidebar menu area end */}
        {/* main content area start */}
        <div className="main-content">
          {/* header area start */}
          <Header />
          {/* header area end */}
          {/* page title area start */}
          <SubHeader />
          {/* page title area end */}
          <div className="main-content-inner">
            <div className="row">
              <div className="col-12 mt-3">
                {children}
              </div>
            </div>
          </div>
        </div>
        {/* main content area end */}
        {/* footer area start*/}
        <Footer />
        {/* footer area end*/}
      </div>
      <SidebarRight />
    </>
  )
}

export default connect((state: AppState) => state.system, { logout, block })(Layout)
