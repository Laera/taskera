import React, { ReactElement, useEffect, useState } from 'react'
import menuDefinition, { MenuItem } from '../../../config/menu';
import { withRouter, Link } from 'react-router-dom';
import { getHierarchyMenuListByPathRecursively } from '../../../helpers/menu.helper';

interface Props {

}

function Breadcrumb(props: any): ReactElement {
  const [menuItem, setMenuItem] = useState<MenuItem>(null)
  const [menuItemHierarchyList, setMenuItemHierarchyList] = useState<MenuItem[]>(null)

  useEffect(() => {
    const menuItems = getHierarchyMenuListByPathRecursively(props.location.pathname);

    if (menuItems && menuItems.length) {

      if (menuItems.length === 1 && menuItems[0].path !== '/home') {
        menuItems.unshift(menuDefinition[0]);
      }

      setMenuItem(menuItems[menuItems.length - 1])
      setMenuItemHierarchyList(menuItems)
    } else {
      setMenuItemHierarchyList(null);
      setMenuItem(null)
    }
  }, [props.location.pathname])

  return (
    <div className="breadcrumbs-area clearfix">
      {
        menuItem &&
        <h4 className="page-title pull-left">{menuItem.label}</h4>
      }
      {
        menuItemHierarchyList &&
        <ul className="breadcrumbs pull-left">
          {
            menuItemHierarchyList.map((menuItem, menuItemIndex) => (
              menuItemIndex === menuItemHierarchyList.length - 1 ?
                <li key={menuItemIndex} ><span>{menuItem.label}</span></li>
                :
                <li key={menuItemIndex} ><Link to={menuItem.path || ''}>{menuItem.label}</Link></li>
            ))
          }
        </ul>
      }
    </div>
  )
}

export default withRouter(Breadcrumb)