import React, { ReactElement } from 'react'

interface Props {

}

function Alerts({ }: Props): ReactElement {
  return (
    <li className="dropdown">
      <i className="ti-bell dropdown-toggle" data-toggle="dropdown">
        <span>2</span>
      </i>
      <div className="dropdown-menu bell-notify-box notify-box">
        <span className="notify-title">You have 3 new notifications <a href="#">view all</a></span>
        <div className="nofity-list">
          <a href="#" className="notify-item">
            <div className="notify-thumb"><i className="ti-key btn-danger" /></div>
            <div className="notify-text">
              <p>You have Changed Your Password</p>
              <span>Just Now</span>
            </div>
          </a>
          <a href="#" className="notify-item">
            <div className="notify-thumb"><i className="ti-comments-smiley btn-info" /></div>
            <div className="notify-text">
              <p>New Commetns On Post</p>
              <span>30 Seconds ago</span>
            </div>
          </a>
          <a href="#" className="notify-item">
            <div className="notify-thumb"><i className="ti-key btn-primary" /></div>
            <div className="notify-text">
              <p>Some special like you</p>
              <span>Just Now</span>
            </div>
          </a>
        </div>
      </div>
    </li>
  )
}

export default Alerts
