import React, { ReactElement } from 'react'
import Breadcrumb from './Breadcrumb'
import User from './User'

interface Props {

}

export default function SubHeader({ }: Props): ReactElement {
  return (
    <div className="page-title-area">
      <div className="row align-items-center">
        <div className="col-sm-6">
          <Breadcrumb />
        </div>
        <div className="col-sm-6 clearfix">
          <User />
        </div>
      </div>
    </div>
  )
}
