import React, { ReactElement } from 'react';
import Alerts from './Alerts';
import Messages from './Messages';
import SearchBox from './SearchBox';

interface Props {

}

function Header({ }: Props): ReactElement {
  return (
    <div className="header-area">
      <div className="row align-items-center">
        {/* nav and search button */}
        <div className="col-md-6 col-sm-8 clearfix">
          <div className="nav-btn pull-left">
            <span />
            <span />
            <span />
          </div>
          <SearchBox />
        </div>
        {/* profile info & task notification */}
        <div className="col-md-6 col-sm-4 clearfix">
          <ul className="notification-area pull-right">
            <li id="full-view"><i className="ti-fullscreen" /></li>
            <li id="full-view-exit"><i className="ti-zoom-out" /></li>
            <Alerts />
            <Messages />
            <li className="settings-btn">
              <i className="ti-settings" />
            </li>
          </ul>
        </div>
      </div>
    </div>

  )
}

export default Header
