import React, { ReactElement } from 'react'
import { AppState } from '../../../store'
import { logout } from '../../../store/system/actions'
import { connect } from 'react-redux'
import { SystemState } from '../../../store/system/types'
import { toast } from 'react-toastify'
import { Link } from 'react-router-dom'

interface Props extends SystemState {
  clearUser: typeof logout
}

function User({ clearUser, user }: Props): ReactElement {
  function handleLogout(event: any) {
    event.preventDefault();
    toast.info('Até logo!');
    clearUser();
  }

  return (
    <div className="user-profile pull-right">
      {
        user.caminhoImagem &&
        <img className="avatar user-thumb" src={user.caminhoImagem} alt="avatar" />
      }
      <h4 className="user-name dropdown-toggle" data-toggle="dropdown">{user.nome} <i className="fa fa-angle-down" /></h4>
      <div className="dropdown-menu">
        <Link to="/perfil" className="dropdown-item">Perfil</Link>
        <Link to="/profile" className="dropdown-item">Confiugurações</Link>
        <a className="dropdown-item" onClick={handleLogout} href="#">Sair</a>
      </div>
    </div>
  )
}

export default connect((state: AppState) => state.system, { clearUser: logout })(User)
