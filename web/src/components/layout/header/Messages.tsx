import React, { ReactElement } from 'react';

interface Props {

}

function Messages({ }: Props): ReactElement {
  return (
    <li className="dropdown">
      <i className="fa fa-envelope-o dropdown-toggle" data-toggle="dropdown"><span>3</span></i>
      <div className="dropdown-menu notify-box nt-enveloper-box">
        <span className="notify-title">You have 3 new notifications <a href="#">view all</a></span>
        <div className="nofity-list">
          <a href="#" className="notify-item">
            <div className="notify-thumb">
              <img src="assets/images/author/author-img1.jpg" alt="image" />
            </div>
            <div className="notify-text">
              <p>Aglae Mayer</p>
              <span className="msg">Hey I am waiting for you...</span>
              <span>3:15 PM</span>
            </div>
          </a>
        </div>
      </div>
    </li>
  )
}

export default Messages
