import React, { ReactElement } from 'react'

interface Props {

}

export default function SearchBox({ }: Props): ReactElement {
  return (
    <div className="search-box pull-left">
      <form action="#">
        <input type="text" name="search" placeholder="Procurar" required />
        <i className="ti-search" />
      </form>
    </div>
  )
}
