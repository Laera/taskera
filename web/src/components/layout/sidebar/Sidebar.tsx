import React, { ReactElement } from 'react'
import Menu from './Menu';
import { Link } from 'react-router-dom';

interface Props {

}

function Sidebar(): ReactElement {
  return (
    <div className="sidebar-menu">
      <div className="sidebar-header">
        <div className="logo">
          <h3>TASKERA</h3>
          {/* <a href="#"><img src="assets/images/icon/logo.png" alt="logo" /></a> */}
        </div>
      </div>
      <Menu />
    </div>
  )
}

export default Sidebar
