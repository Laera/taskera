import React from 'react'
import { Link, withRouter, RouteComponentProps } from 'react-router-dom'

interface Props extends RouteComponentProps {
  path: string;
  label: string;
  icon?: string;
  childRoutes?: { label: string, path: string }[]
}

const MenuLink: React.FC<Props> = ({ path, label, icon, location, childRoutes }: Props) => {
  let className: string = '';
  let linkClassName: string = '';
  let ulClassName: string = '';

  if (location.pathname.startsWith(path)) {
    className = className.concat(' ', 'mm-active');
  }

  if (childRoutes && childRoutes.length) {
    ulClassName = ulClassName.concat('mm-collapse mm-show');
  }

  return (
    <li className={className}>
      {
        childRoutes ?
          <a href="" onClick={event => event.preventDefault()} aria-expanded="true">
            <i className={icon} />
            <span>{label}</span>
          </a>
          :
          <Link className={linkClassName} to={path}>
            <i className={icon} />
            <span>{label}</span>
          </Link>
      }
      {
        childRoutes &&
        <ul className={ulClassName}>
          {
            childRoutes.map((childRoute, childRouteIndex) => {
              let childLinkClassName: string = '';

              if (location.pathname === childRoute.path) {
                childLinkClassName = childLinkClassName.concat('active');
              }

              return (
                <li key={childRouteIndex} className={childLinkClassName}>
                  <Link  to={childRoute.path} key={childRouteIndex} >{childRoute.label}</Link>
                </li>
              )

            })
          }
        </ul>

      }
    </li>
  )
}

export default withRouter(MenuLink)
