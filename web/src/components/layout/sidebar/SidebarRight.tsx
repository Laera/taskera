import React, { ReactElement } from 'react'

interface Props {

}

export default function SidebarRight({ }: Props): ReactElement {
  return (
    <div className="offset-area">
      <div className="offset-close"><i className="ti-close" /></div>
      <ul className="nav offset-menu-tab">
        <li><a className="active" data-toggle="tab" href="#activity">Activity</a></li>
        <li><a data-toggle="tab" href="#settings">Settings</a></li>
      </ul>
      <div className="offset-content tab-content">
        <div id="activity" className="tab-pane fade in show active">
          <div className="recent-activity">
            <div className="timeline-task">
              <div className="icon bg1">
                <i className="fa fa-envelope" />
              </div>
              <div className="tm-title">
                <h4>Rashed sent you an email</h4>
                <span className="time"><i className="ti-time" />09:35</span>
              </div>
              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Esse distinctio itaque at.
      </p>
            </div>
            <div className="timeline-task">
              <div className="icon bg2">
                <i className="fa fa-check" />
              </div>
              <div className="tm-title">
                <h4>Added</h4>
                <span className="time"><i className="ti-time" />7 Minutes Ago</span>
              </div>
              <p>Lorem ipsum dolor sit amet consectetur.
      </p>
            </div>
            <div className="timeline-task">
              <div className="icon bg2">
                <i className="fa fa-exclamation-triangle" />
              </div>
              <div className="tm-title">
                <h4>You missed you Password!</h4>
                <span className="time"><i className="ti-time" />09:20 Am</span>
              </div>
            </div>
            <div className="timeline-task">
              <div className="icon bg3">
                <i className="fa fa-bomb" />
              </div>
              <div className="tm-title">
                <h4>Member waiting for you Attention</h4>
                <span className="time"><i className="ti-time" />09:35</span>
              </div>
              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Esse distinctio itaque at.
      </p>
            </div>
          </div>
        </div>
        <div id="settings" className="tab-pane fade">
          <div className="offset-settings">
            <h4>General Settings</h4>
            <div className="settings-list">
              <div className="s-settings">
                <div className="s-sw-title">
                  <h5>Notifications</h5>
                  <div className="s-swtich">
                    <input type="checkbox" id="switch1" />
                    <label htmlFor="switch1">Toggle</label>
                  </div>
                </div>
                <p>Keep it 'On' When you want to get all the notification.</p>
              </div>
              <div className="s-settings">
                <div className="s-sw-title">
                  <h5>Show recent activity</h5>
                  <div className="s-swtich">
                    <input type="checkbox" id="switch2" />
                    <label htmlFor="switch2">Toggle</label>
                  </div>
                </div>
                <p>The for attribute is necessary to bind our custom checkbox with the input.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
