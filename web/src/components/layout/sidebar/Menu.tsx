import React, { ReactElement, useEffect, useRef } from 'react'
import MenuLink from './MenuLink'
import MetisMenu from 'metismenujs';
import menuDefinition from '../../../config/menu';

interface Props {

}

function Menu(): ReactElement {
  const metisMenuRef = useRef(null);

  useEffect(() => {
    metisMenuRef.current = new MetisMenu('#menu');

    return () => {
      metisMenuRef.current.dispose();
    }
  }, [])

  return (
    <div className="main-menu">
      <div className="menu-inner">
        <nav>
          <ul className="metismenu" id="menu">
            {
              menuDefinition.filter(item => !item.hide).map((item, index) => (
                <MenuLink key={index} path={item.path} label={item.label} icon={item.icon} childRoutes={item.subItems as any} />
              ))
            }
          </ul>
        </nav>
      </div>
    </div>
  )
}

export default Menu
