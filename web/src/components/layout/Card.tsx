import React, { ReactElement } from 'react';

type CardDropdownOptions = {
  title: string,
  separator?: boolean,
  handler?: () => void,
}[]

interface Props {
  title: string,
  children: ReactElement | ReactElement[],
  dropdownOptions?: { header: string, options: CardDropdownOptions }
}

function Card({ title, children, dropdownOptions }: Props): ReactElement {
  return (
    <div className="card">
      <div className="card-body">
        {
          title &&
          <h4 className="header-title">{title}</h4>
        }
        {children}
      </div>
    </div>
  )
}

export default Card
