import React, { ReactElement } from 'react'

interface Props {

}

function Footer({ }: Props): ReactElement {
  return (
    <footer>
      <div className="footer-area">
        <p>© Copyright 2018. All right reserved.</p>
      </div>
    </footer>
  )
}

export default Footer
