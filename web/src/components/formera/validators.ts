import { ValidatorFunction, ValidationMessageSource, ValidationFunctionSource } from 'formera-form';
import { get } from 'formera-form/dist/utils';

const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})/

export const validationMessages: ValidationMessageSource = {
  email: 'O email precisa ser válido.',
  required: 'O valor é obrigatório.',
  password: 'A senha precisa conter pelo menos uma letra maiuscula, uma letra miniscula, um numero e 8 caracteres.',
  passwordConfirmation: 'O valor não é válido.'
}

export const validationFunctions: ValidationFunctionSource = {
  password: (fieldState, formValues) => {
    const { value } = fieldState;

    if (!passwordRegex.test(value)) {
      return validationMessages.password;
    }

    return null;
  },
  equals: (fieldState, formValues, [field, message]) => {
    const { value } = fieldState;

    if (value !== get(formValues, field)) {
      return message || validationMessages.passwordConfirmation;
    }

    return null;
  }
}