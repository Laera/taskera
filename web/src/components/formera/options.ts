import { Customizable } from "formera-form-react-builder";

const options: Partial<Customizable> = {
  sendClassName: 'btn btn-xs btn-success btn-flat',
  cancelClassName: 'btn btn-xs btn-light btn-flat',
  addItemClassName: 'btn btn-xs btn-success btn-flat',
  removeItemClassName: 'btn btn-xs btn-danger btn-flat'
}

export default options;