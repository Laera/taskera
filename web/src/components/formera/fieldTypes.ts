import Default from "../inputs/Default";
import { FieldTypes } from "formera-form-react-builder";
import Select from "../inputs/Select";

export default {
  input: Default,
  select: Select
} as any