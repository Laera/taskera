import React, { ReactElement } from 'react';
import { Input } from 'formera-form-react';

function Default({ onFocus, onChange, onBlur, value }: Input): ReactElement {
  return (
    <input type="text"
      className="form-control form-control-sm"
      value={value}
      onFocus={onFocus}
      onChange={onChange}
      onBlur={onBlur}
    />
  )
}

export default Default

