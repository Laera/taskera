import React, { ReactElement, PureComponent } from 'react';
import ReactSelect from 'react-select';
import withFormera from 'formera-form-react-builder/dist/formera-form-react/src/withFormera';
import './Select.css';

interface State {
  loading: boolean
}

class Select extends PureComponent<any, State> {
  constructor(props: any) {
    super(props);

    this.state = { loading: true }
  }

  async componentDidMount() {
  }

  render() {
    const { loading } = this.state;

    return (
      <ReactSelect {...this.props as any}
        isLoading={loading}
        isClearable={true}
        classNamePrefix="react-select"
        placeholder="Selecione..."
        noOptionsMessage={() => 'Não há itens'}
        loadingMessage={() => 'Carregando...'}
      />
    )
  }
}

export default withFormera(Select)
