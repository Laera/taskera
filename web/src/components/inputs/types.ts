export interface Query {
  service: string,
  method?: string,
  params?: any
}