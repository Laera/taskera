import BaseService from "../../services/BaseService";

export interface QueryDefinition {
  service: string,
  method?: string,
  params?: any
  isProtected?: string,
}

/**Executa query da api. */
export async function doQuery(queryDefinition: QueryDefinition): Promise<any> {
  const { service, method, params, isProtected } = queryDefinition;
  const serviceInstance = new BaseService(service, !!isProtected);


  
  return null;
}