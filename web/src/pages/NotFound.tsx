import React, { ReactElement } from 'react';
import { Link } from 'react-router-dom';

interface Props {

}

function NotFound(): ReactElement {
  return (
    <div className="container text-center">
      <div className="error-content">
        <h2>404</h2>
        <p>Página não encontrada.</p>
        <Link to="/home">Voltar para a página inicial</Link>
      </div>
    </div>
  )
}

export default NotFound
