import React, { ReactElement } from 'react'
import { Field } from 'formera-form-react'
import { FieldValidator } from 'formera-form'

interface Props {
  name: string,
  title: string,
  icon: string,
  type?: string,
  validators?: Array<string | FieldValidator>,
}

function AuthField({ name, validators, title, icon, type = 'text' }: Props): ReactElement {
  return (
    <Field name={name} validators={validators}>
      {({ input, meta }) => (
        <div className="form-gp">
          <label>{title}</label>
          <input {...input} type={type} />
          <i className={icon} />
          {
            meta.touched && meta.error &&
            <div className="text-danger" >{meta.error} </div >
          }
        </div>
      )}
    </Field>
  )
}

export default AuthField
