import React, { ReactElement, useEffect, useLayoutEffect } from 'react'
import initialize from '../../strdash/js/login';
import FullLoading from '../../components/shared/FullLoading';
import AuthField from './AuthField';
import { Link } from 'react-router-dom';
import { connect, DispatchProp } from 'react-redux';
import { SystemState } from '../../store/system/types';
import { AppState } from '../../store';
import { login, load } from '../../store/system/actions';
import { toast } from 'react-toastify';
import { Form } from 'formera-form-react';
import { validationFunctions, validationMessages } from '../../components/formera/validators';
import { UnauthenticatedSessionControl } from 'react-session-control';

interface Props extends SystemState, DispatchProp {
  login: typeof login,
  load: typeof load
}

function Login({ login, load, error, loading }: Props): ReactElement {
  useEffect(() => {
    if (error) {
      toast.error(error.response.data);
    }
  }, [error]);

  useLayoutEffect(() => {
    initialize();
  }, [])

  async function handleLogin({ values }: any) {
    const { email, password } = values;
    login(email, password);
  }

  return <div className="login-area">
    <div className="container">
      <div className="login-box ptb--100">
        <UnauthenticatedSessionControl storageTokenKey="token" onLogin={() => load()} />

        <Form onSubmit={handleLogin}
          initialValues={{}}
          validationType="onBlur"
          customValidators={validationFunctions}
          customValidationMessages={validationMessages} >
          {({ valid, values }, formera) => (
            <>
              <div className="login-form-head">
                <h4>Entrar</h4>
                <p>Olá! Bem vindo ao taskera. Entre com seus dados para iniciar!</p>
              </div>
              <div className="login-form-body">
                <AuthField name="email" title="Email" icon="ti-email" validators={['required', 'email']} />
                <AuthField name="password" title="Senha" type="password" icon="ti-lock" validators={['required', 'password']} />
                <div className="row mb-4 rmber-area">
                  <div className="col-6"></div>
                  <div className="col-span-6 col-6 text-right">
                    <Link to="/recuperar-senha">Esqueceu sua senha?</Link>
                  </div>
                </div>
                <div className="submit-btn-area">
                  <button id="form_submit" type="submit">Entrar <i className="ti-arrow-right" /></button>
                </div>
                <div className="form-footer text-center mt-5">
                  <p className="text-muted">Não tem uma conta? <Link to="/registrar">Registrar</Link></p>
                </div>
              </div>
            </>
          )}
        </Form>
      </div>
    </div>
    {
      loading &&
      <div className="overlay">
        <FullLoading></FullLoading>
      </div>
    }
  </div>
}

export default connect((state: AppState) => state.system, { login, load })(Login)
