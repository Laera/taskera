import React, { ReactElement, useState, useLayoutEffect, useEffect } from 'react'
import authService from '../../services/auth.service';
import initialize from '../../strdash/js/login';
import FullLoading from '../../components/shared/FullLoading';
import AuthField from './AuthField';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import { history } from '../../routes/Routes';
import { Form } from 'formera-form-react';
import { validationFunctions, validationMessages } from '../../components/formera/validators';

function Register(props: any): ReactElement {
  const [loading, setLoading] = useState<Boolean>(false);

  async function handleRegister({ values }: any) {
    try {
      setLoading(true);
      await authService.register(values);
      toast.success('Um link de confirmação foi enviado ao seu email.');
      history.push('/login');
    } catch (error) {
      toast.error(error.response.data);
      setLoading(false);
    }
  }

  useEffect(() => {
    initialize();
  }, []);

  return (
    <div className="login-area">
      <div className="container">
        <div className="login-box ptb--100">
          <Form onSubmit={handleRegister}
            initialValues={{}}
            validationType="onBlur"
            customValidators={validationFunctions}
            customValidationMessages={validationMessages} >
            {(formState) => (
              <>
                <div className="login-form-head">
                  <h4>Registrar</h4>
                  <p>Olá, cadastra-se!</p>
                </div>
                <div className="login-form-body">
                  <AuthField name="nome" title="Nome completo" icon="ti-user" validators={['required']} />
                  <AuthField name="email" title="Email" icon="ti-email" validators={['required', 'email']} />
                  <AuthField name="senha" type="password" title="Senha" icon="ti-lock" validators={['required', 'password']} />
                  <AuthField name="confirmacaoSenha" type="password" title="Confirmação da senha" icon="ti-lock"
                    validators={['required', { name: 'equals', params: ['senha', 'A confirmação da senha deve ser igual a senha.'] as any }]} />
                  <div className="submit-btn-area">
                    <button id="form_submit" type="submit">Registrar <i className="ti-arrow-right" /></button>
                  </div>
                  <div className="form-footer text-center mt-5">
                    <p className="text-muted">Já tem uma conta? <Link to="/login">Registrar</Link></p>
                  </div>
                </div>
              </>
            )}
          </Form>
        </div>
      </div>
      {
        loading &&
        <div className="overlay">
          <FullLoading></FullLoading>
        </div>
      }
    </div>
  )
}

export default Register 
