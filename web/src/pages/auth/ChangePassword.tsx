import React, { ReactElement, useState, useLayoutEffect, useEffect } from 'react'
import authService from '../../services/auth.service';
import initialize from '../../strdash/js/login';
import FullLoading from '../../components/shared/FullLoading';
import AuthField from './AuthField';
import { toast } from 'react-toastify';
import { history } from '../../routes/Routes';
import { withRouter } from 'react-router-dom';
import { Form } from 'formera-form-react';
import { validationFunctions, validationMessages } from '../../components/formera/validators';

function ChangePassword(props: any): ReactElement {
  const [loading, setLoading] = useState<Boolean>(false);

  async function handleChangePassword({ values }: any) {
    try {
      setLoading(true);
      await authService.changePassword(props.match.params.hash, values.senha, values.confirmacaoSenha);
      toast.success('Senha atualizada com sucesso.');
      history.push('/login');

    } catch (error) {
      toast.error(error.response.data);
      setLoading(false);
    }
  }

  useEffect(() => {
    initialize();
  }, []);

  return (
    <div className="login-area">
      <div className="container">
        <div className="login-box ptb--100">
          <Form onSubmit={handleChangePassword}
            initialValues={{}}
            validationType="onBlur"
            customValidators={validationFunctions}
            customValidationMessages={validationMessages} >
            {(formState) => (
              <>
                <div className="login-form-head">
                  <h4>Trocar senha</h4>
                  <p>Insira sua nova senha e a confirme para recuperar seu login.</p>
                </div>
                <div className="login-form-body">
                  <AuthField name="senha" type="password" title="Senha" icon="ti-lock" validators={['required', 'password']} />
                  <AuthField name="confirmacaoSenha" type="password" title="Confirmação da senha" icon="ti-lock"
                    validators={['required', { name: 'equals', params: ['senha', 'A confirmação da senha deve ser igual a senha.'] as any }]} />
                  <div className="submit-btn-area">
                    <button id="form_submit" type="submit">Trocar senha <i className="ti-arrow-right" /></button>
                  </div>
                </div>

              </>
            )}
          </Form>
        </div>
      </div>
      {
        loading &&
        <div className="overlay">
          <FullLoading></FullLoading>
        </div>
      }
    </div>

  )
}

export default withRouter(ChangePassword)
