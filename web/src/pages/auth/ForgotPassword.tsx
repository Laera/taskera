import React, { ReactElement, useState, useEffect } from 'react'
import authService from '../../services/auth.service';
import initialize from '../../strdash/js/login';
import FullLoading from '../../components/shared/FullLoading';
import AuthField from './AuthField';
import { toast } from 'react-toastify';
import { history } from '../../routes/Routes';
import { Form } from 'formera-form-react';
import { validationFunctions, validationMessages } from '../../components/formera/validators';

function ForgotPassword(props: any): ReactElement {
  const [loading, setLoading] = useState<Boolean>(false);

  async function handleForgotPassword({ values }: any) {
    try {
      setLoading(true);
      await authService.changePasswordRequest(values.email);
      toast.success('Um link para atualização de senha foi enviado ao seu email.');
      history.push('/login');
    } catch (error) {
      toast.error(error.response.data);
      setLoading(false);
    }
  }

  useEffect(() => {
    initialize();
  }, []);

  return (
    <div className="login-area">
      <div className="container">
        <div className="login-box ptb--100">
          <Form onSubmit={handleForgotPassword}
            initialValues={{}}
            validationType="onBlur"
            customValidators={validationFunctions}
            customValidationMessages={validationMessages} >
            {(formState) => (
              <>
                <div className="login-form-head">
                  <h4>Recuperação de senha</h4>
                  <p>Perdeu sua senha? Informe seu email para resetar sua senha.</p>
                </div>
                <div className="login-form-body">
                  <AuthField name="email" title="Email" icon="ti-email" validators={['required', 'email']} />
                  <div className="submit-btn-area">
                    <button id="form_submit" type="submit">Esqueci minha senha <i className="ti-arrow-right" /></button>
                  </div>
                </div>

              </>
            )} 
            </Form>
        </div>
      </div>
      {
        loading &&
        <div className="overlay">
          <FullLoading></FullLoading>
        </div>
      }
    </div>

  )
}

export default ForgotPassword 
