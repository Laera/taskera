import React, { ReactElement, useState, useEffect, useLayoutEffect } from 'react'
import initialize from '../../strdash/js/login';
import FullLoading from '../../components/shared/FullLoading';
import AuthField from './AuthField';
import { connect, DispatchProp } from 'react-redux';
import { SystemState } from '../../store/system/types';
import { AppState } from '../../store';
import { load, unblock } from '../../store/system/actions';
import { toast } from 'react-toastify';
import { history } from '../../routes/Routes';
import { Form } from 'formera-form-react';
import { validationFunctions, validationMessages } from '../../components/formera/validators';
import { UnauthenticatedSessionControl } from 'react-session-control';
import { RouteComponentProps } from 'react-router-dom';

interface Props extends SystemState, DispatchProp, RouteComponentProps {
  unblock: typeof unblock,
  load: typeof load
}

function Lock({ block, unblock, load, user, loading, error, location }: Props): ReactElement {
  const [initialValues, setInitialValues] = useState({});
  useEffect(() => {
    if (!block) {
      history.replace('/login');
      return;
    }

    setInitialValues({ email: user.email });
  }, []);

  useEffect(() => {
    if (error) {
      toast.error(error.response.data);
    }
  }, [error])

  useLayoutEffect(() => {
    initialize();
  }, [])

  async function handleUnblock({ values }: any) {
    const { email, password } = values;
    const path = (location.state && location.state.backTo) || '/home';
    unblock(email, password, path.replace('/taskera', ''));
  }

  return <div className="login-area">
    <div className="container">
      <div className="login-box ptb--100">
        <UnauthenticatedSessionControl storageTokenKey="token" onLogin={() => load()} />
        <Form onSubmit={handleUnblock}
          initialValues={initialValues}
          validationType="onBlur"
          customValidators={validationFunctions}
          customValidationMessages={validationMessages} >
          {({ valid, values }, formera) => (
            <>
              <div className="login-form-head">
                <h4>Bloqueado</h4>
                <p>Parece que você ficou muito tempo inativo. Entre com sua senha para retornar.</p>
              </div>
              <div className="login-form-body">
                <AuthField name="password" title="Senha" type="password" icon="ti-lock" validators={['required', 'password']} />
                <div className="submit-btn-area">
                  <button id="form_submit" type="submit">Desbloquear <i className="ti-arrow-right" /></button>
                </div>
              </div>
            </>
          )}
        </Form>
      </div>
    </div>
    {
      loading &&
      <div className="overlay">
        <FullLoading></FullLoading>
      </div>
    }
  </div>
}

export default connect((state: AppState) => state.system, { unblock, load })(Lock)
