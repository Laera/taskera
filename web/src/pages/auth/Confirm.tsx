import React, { ReactElement, useEffect } from 'react'
import Loading from '../../components/shared/Loading'
import { withRouter, RouteComponentProps } from 'react-router-dom'
import { connect } from 'react-redux'
import { confirm } from '../../store/system/actions'
import { SystemState } from '../../store/system/types'

interface Props extends SystemState, RouteComponentProps<{ hash: string }> {
  confirm: typeof confirm
}

function Confirm({ match, confirm }: Props): ReactElement {
  useEffect(() => {
    confirm(match.params.hash);
  }, [])

  return (
    <div className="login-area">
      <div className="container">
        <div className="login-box ptb--100">
          <form>
            <div className="login-form-head">
              <h4>Confirmação de cadastro</h4>
              <p>Aguarde um momento estamos confirmando seu cadastro!</p>
            </div>
            <div className="login-form-body">
              <Loading />
            </div>
          </form>
        </div>
      </div>
    </div>
  )
}

export default withRouter(connect(state => state, { confirm })(Confirm));
