import React, { ReactElement } from 'react'
import { Link } from 'react-router-dom'

interface Props {

}

export default function Forbidden({ }: Props): ReactElement {
  return (
    <div className="error-content text-center">
      <h2>403</h2>
      <p>Você não tem permissão para acessar esta página.</p>
      <Link to="/home">Voltar para a página inicial</Link>
    </div>

  )
}
