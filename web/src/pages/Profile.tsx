import React, { ReactElement } from 'react';
import Card from '../components/layout/Card';
import FormeraBuilder, { Form } from 'formera-form-react-builder';
import fieldTypes from '../components/formera/fieldTypes';
import options from '../components/formera/options';
import { validationMessages, validationFunctions } from '../components/formera/validators';

interface Props {

}

const formDefinition: Partial<Form> = {
  name: 'usuario',
  fields: [
    {
      name: 'nome',
      title: 'Nome',
      type: 'input',
      sizeClass: 'col-sm-12',
      validators: ['required']
    },
    {
      name: 'email',
      title: 'Email',
      type: 'input',
      sizeClass: 'col-sm-12',
      validators: ['required', { name: 'email', debounce: 500 }]
    },
    {
      name: 'sexo',
      title: 'Sexo',
      type: 'select',
      sizeClass: 'col-sm-6',
      validators: ['required'],
    },
    {
      title: 'TEste12321321',
      name: 'teste',
      multiple: true,
      type: null,
      sizeClass: 'col-12',
      fields: [
        {
          name: 'email',
          title: 'Email',
          type: 'input',
          sizeClass: 'col-sm-12',
          validators: ['required', { name: 'email', debounce: 500 }]
        },
        {
          name: 'sexo',
          title: 'Sexo',
          type: 'select',
          sizeClass: 'col-sm-6',
          validators: ['required'],
        },
      ]
    }
  ]
}

function Profile({ }: Props): ReactElement {
  return (
    <div className="row">
      <div className="col-12">
        <Card title="Geral">
          <FormeraBuilder
            debug={false}
            options={options}
            initialValues={{}}
            showCancelButton={false}
            validationType="onChange"
            formDefinition={formDefinition as Form}
            customValidators={validationFunctions}
            customValidationMessages={validationMessages}
            fieldTypes={fieldTypes}
            onSubmit={values => console.log('values :', values)}
          />
        </Card>
      </div>
    </div>

  )
}

export default Profile
