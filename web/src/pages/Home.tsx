import React, { ReactElement, useEffect, useLayoutEffect } from 'react';
import Card from '../components/layout/Card';

interface Props {

}

const list = [
  {
    text: 'teste1',
    childrens: [
      {
        text: 'teste1-1'
      },
      {
        text: 'teste1-2'
      },
      {
        text: 'teste1-3'
      }
    ]
  },
  {
    text: 'teste2',
    childrens: [
      {
        text: 'teste2-1'
      },
      {
        text: 'teste2-2'
      }
    ]
  },
  {
    text: 'teste3',
    childrens: [
      {
        text: 'teste3-1'
      },
      {
        text: 'teste3-2'
      }
    ]
  },
  {
    text: 'teste4',
    childrens: [
      {
        text: 'teste4-1'
      },
      {
        text: 'teste4-2'
      }
    ]
  }
]

function Home({ }: Props): ReactElement {

  return (
    <div className="row">
      <div className="col-lg-12">
        <Card title="Teste">
          <div className="row">
            <div className="col-6">
            </div>
          </div>
        </Card>
      </div>
    </div>
  )
}

export default Home
