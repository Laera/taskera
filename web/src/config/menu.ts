export interface MenuItem {
  label: string,
  path?: string,
  icon?: string,
  hide?: boolean,
  subItems?: MenuItem[]
}

export type MenuDefinition = MenuItem[];

const menuDefinition: MenuDefinition = [
  {
    label: 'Home',
    path: '/home',
    icon: 'fa fa-dashboard',
  },
  {
    label: 'Cadastros',
    icon: 'fa fa-edit',
    subItems: [
      {
        label: 'Colaboradores',
        path: '/colaboradores',
      },
      {
        label: 'Alunos',
        path: '/alunos',
      },
      {
        label: 'Turmas',
        path: '/turmas',
      },
      {
        label: 'Disciplinas',
        path: '/disciplinas',
      }
    ]
  },
  //Parte escondida.
  {
    label: '404',
    path: '/404',
    hide: true,
  },
  {
    label: '403',
    path: '/403',
    hide: true,
  },
  {
    label: 'Perfil',
    path: '/perfil',
    hide: true,
  },
];

export default menuDefinition;