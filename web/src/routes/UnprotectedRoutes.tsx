import React, { ReactElement } from 'react'
import Login from '../pages/auth/Login'
import ForgotPassword from '../pages/auth/ForgotPassword'
import Register from '../pages/auth/Register'
import Confirm from '../pages/auth/Confirm'
import ChangePassword from '../pages/auth/ChangePassword'
import Lock from '../pages/auth/Lock'
import { Switch, Route, Redirect } from 'react-router'

function UnprotectedRoutes(): ReactElement {
  return (
    <Switch>
      <Route path="/" exact>
        <Redirect to="/login" />
      </Route>
      <Route path="/login" component={Login} />
      <Route path="/lock" component={Lock} />
      <Route path="/registrar" component={Register} />
      <Route path="/confirmar/:hash" component={Confirm} />
      <Route path="/recuperar-senha" component={ForgotPassword} />
      <Route path="/trocar-senha/:hash" component={ChangePassword} />
      <Route path="*" >
        <Redirect to="/login" />
      </Route>
    </Switch >
  )
}

export default UnprotectedRoutes
