import React, { ReactElement, useEffect } from 'react'
import Layout from '../components/layout/Layout'
import Home from '../pages/Home'
import NotFound from '../pages/NotFound'
import FullLoading from '../components/shared/FullLoading'
import Profile from '../pages/Profile'
import Forbidden from '../pages/Forbidden'
import { Switch, Route, Redirect } from 'react-router'
import { connect, DispatchProp } from 'react-redux'
import { AppState } from '../store'
import { load } from '../store/system/actions'

interface Props extends AppState, DispatchProp { }

function ProtectedRoutes({ system: { user }, dispatch }: Props): ReactElement {
  useEffect(() => {
    dispatch(load() as any);
  }, []);

  return (
    user ?
      <Layout>
        <Switch>
          <Route path="/" exact>
            <Redirect to="/home" />
          </Route>
          <Route path="/home">
            <Home />
          </Route>
          <Route path="/perfil">
            <Profile />
          </Route>
          <Route path="/login">
            <Redirect to="/home" />
          </Route>
          <Route path="/404">
            <NotFound />
          </Route>
          <Route path="/403">
            <Forbidden />
          </Route>
          <Route path="*">
            <Redirect to="/404" />
          </Route>
        </Switch>
      </Layout>
      :
      <FullLoading />
  )
}

export default connect((state: AppState) => state)(ProtectedRoutes)
