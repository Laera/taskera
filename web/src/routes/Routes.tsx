import React, { ReactElement, Suspense } from 'react';
import authService from '../services/auth.service';
import UnprotectedRoutes from './UnprotectedRoutes';
import ProtectedRoutes from './ProtectedRoutes';
import { Router } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import { AppState } from '../store';
import { connect } from 'react-redux';

export const history = createBrowserHistory({ basename: '/taskera' });

function Routes(props: any): ReactElement {
  return (
    <Router history={history}>
      {
        authService.isAuthenticated() ?
          <ProtectedRoutes />
          :
          <UnprotectedRoutes />
      }
    </Router>
  )
}

export default connect((state: AppState) => state.system)(Routes)
