import nodemailer from 'nodemailer';
import config from '../config';

const transporter = nodemailer.createTransport(config.email);

export async function sendConfirmationEmail(email: string, hash: string) {
  const mailOptions: any = {
    from: config.email.auth.user,
    to: email,
    subject: 'Confirmação de email',
    html: `<p> Aqui está o link de confirmação do Taskera: <a href="${config.email.confirmationUrl}/${hash}">Confirmar</a></p>`
  }

  return new Promise((resolve, reject) => {
    transporter.sendMail(mailOptions, (err, info) => {
      if (err) {
        reject(err);
      } else {
        resolve(info);
      }
    })
  })
}


export async function sendChangePasswordEmail(email: string, hash: string) {
  const mailOptions: any = {
    from: config.email.auth.user,
    to: email,
    subject: 'Troca de senha',
    html: `<p> Aqui está o link de troca de senha do Taskera: <a href="${config.email.changePasswordUrl}/${hash}">Trocar senha</a></p>`
  }

  return new Promise((resolve, reject) => {
    transporter.sendMail(mailOptions, (err, info) => {
      if (err) {
        reject(err);
      } else {
        resolve(info);
      }
    })
  })
}