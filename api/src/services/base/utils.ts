import { Response } from "express";
import { MethodOptions } from './decorators';

/**Retorna erro para frontend. */
export function handleError(res: Response, error: any, status: number = 500) {
  res.status(status).send(error);
}

/**Retorna o caminho do método montado com parametros. */
export function getPathWithParams(initialPath: string, method: string, config: MethodOptions) {
  let path;

  //Montando caminho.
  if (config.path === false) {
    path = initialPath;
  }
  else if (config.path === undefined) {
    path = initialPath + '/' + method;
  } else {
    path = initialPath + '/' + config.path;
  }

  //Montando parametros.
  if (config.params) {
    path = path + '/' + config.params
    .map(item => `:${item}`).join('/');
  }

  return path;
}
