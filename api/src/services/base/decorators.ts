import 'reflect-metadata';

/**Opções para um método express. */
export interface MethodOptions {
  type?: 'USE' | 'GET' | 'POST' | 'PUT' | 'PATCH' | 'DELETE';
  path?: string | boolean;
  log?: boolean;
  defaultMethod?: boolean;
  params?: string[];
}

/**Opções padrão para opções de método. */
const defaultOptions: MethodOptions = {
  type: 'USE',
  path: undefined,
  log: true,
  params: undefined,
  defaultMethod: false,
}

/**Chave da informação de método. */
const methodKey = Symbol('method');

/**Diz se a função é um endpoint. */
export function Method(options: MethodOptions) {
  return Reflect.metadata(methodKey, options);
}

/**Retorna a informação do método. */
export function getMethodInfo(target: any, key: string) {
  const options = Reflect.getMetadata(methodKey, target, key);

  return options ? Object.assign({}, defaultOptions, options) : null;

}
