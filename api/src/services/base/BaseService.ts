import 'reflect-metadata';
import ServiceOptions from "./ServiceOptions";
import AuditoriaModel, { Auditoria } from '../../models/auditorias.model';
import LogType from './LogTypes';
import update from './update';
import * as  queryBuilder from '../query/query';
import { Method, MethodOptions, getMethodInfo } from "./decorators";
import { Response, Request, NextFunction } from "express";
import { Router } from "express-serve-static-core";
import { handleError, getPathWithParams } from "./utils";
import { merge } from 'lodash';
import { Model, Op } from 'sequelize';

type SequelizeModel = (new () => Model<unknown, unknown>) & typeof Model;

/**Opções padrão para configuração de serviço. */
const defaultOptions: ServiceOptions = {
  logDescription: '',
  defaultMethods: {
    create: true,
    update: true,
    get: true,
    delete: true,
    restore: true,
  },
  crudOptions: {
    include: null,
  },
  getOptions: {
    log: true,
    statusCheck: true,
    logError: true,
  },
  createOptions: {
    log: true,
    logError: true,
  },
  updateOptions: {
    log: true,
    logError: true,
  },
  deleteOptions: {
    log: true,
    logError: true,
  },
  restoreOptions: {
    log: true,
    logError: true,
  },
  historyOptions: {
    log: true,
    logError: true,
  },
}

/**Classe de serviço básico. */
export class BaseService<T> {
  constructor(path: string, model: SequelizeModel, options?: ServiceOptions) {
    this.model = model;
    this.path = path;
    this.options = merge({}, defaultOptions, options);
    this.logModel = AuditoriaModel;
  }

  /**Métodos que devem ser roteados. */
  private routedMethods: { [name: string]: MethodOptions };
  /**Caminho do serviço. */
  private path: string;
  /**Model do serviço. */
  protected model: SequelizeModel;
  /**Model de log. */
  protected logModel: SequelizeModel;
  /**Opções do serviço. */
  protected options: ServiceOptions;
  /**Retorna os itens da instância. */
  [key: string]: any;

  @Method({ type: 'POST', path: false, defaultMethod: true })
  protected async create(req: Request, res: Response, next: NextFunction) {
    const { crudOptions, createOptions } = this.options;
    try {
      const include = createOptions.include || crudOptions.include;
      const item = req.body;
      const result = await this.model.create(item, { include }) as any;
      res.json(result);
      if (createOptions.log) this.passLog(res, { registroId: result.id, tipo: LogType.Create, dados: item })
    } catch (error) {
      console.error(error);
      handleError(res, { message: 'POST ERROR', error: error.message });
      if (createOptions.logError) this.passLog(res, { error: error.message, status: 'error', tipo: LogType.Create })
    }
    next();
  }

  @Method({ type: 'PUT', path: false, defaultMethod: true, params: ['id'] })
  protected async update(req: Request, res: Response, next: NextFunction) {
    const { id } = req.params;
    const { crudOptions, updateOptions } = this.options;
    try {
      const include = updateOptions.include || crudOptions.include;
      const item = req.body;
      const result = await update(this.model, item, include);
      res.json(result);
      if (updateOptions.log) this.passLog(res, { dados: item, registroId: id, tipo: LogType.Update })
    } catch (error) {
      handleError(res, { message: 'UPDATE ERROR', error: error.message });
      if (updateOptions.logError) this.passLog(res, { error: error.message, registroId: id, status: 'error', tipo: LogType.Update });
    }
    next();
  }

  @Method({ type: 'POST', path: 'restore', defaultMethod: true, params: ['id'] })
  protected async restore(req: Request, res: Response, next: NextFunction) {
    const { id } = req.params;
    const { restoreOptions } = this.options;
    try {
      const item = req.body;
      const result = await this.model.restore({ where: { id: id } });
      res.json(result);
      if (restoreOptions.log) this.passLog(res, { dados: item, registroId: id, tipo: LogType.Restore })
    } catch (error) {
      handleError(res, { message: 'RESTORE ERROR', error: error.message });
      if (restoreOptions.logError) this.passLog(res, { error: error.message, registroId: id, status: 'error', tipo: LogType.Restore });
    }
    next();
  }

  @Method({ type: 'GET', path: false, defaultMethod: true })
  protected async get(req: Request, res: Response, next: NextFunction) {
    const { getOptions } = this.options;
    try {
      let result;
      const query = queryBuilder.buildQuery(req.query, getOptions);
      const count = await this.model.count(query);
      const rows = await this.model.findAll(query);
      result = { rows, count };
      res.json(result);
      if (getOptions.log) this.passLog(res, { dados: req.query, tipo: LogType.Read })
    } catch (error) {
      console.error(error);
      handleError(res, { message: 'GET ERROR', error: error.message });
      if (getOptions.logError) this.passLog(res, { error: error.message, status: 'error', tipo: LogType.Read });
    }
    next();
  }

  @Method({ type: 'GET', path: 'id', params: ['id'], defaultMethod: true })
  protected async getById(req: Request, res: Response, next: NextFunction) {
    const { id } = req.params;
    const { getOptions, crudOptions } = this.options;
    try {
      const include: any = getOptions.include || crudOptions.include;
      let result;
      if (id) {
        result = await this.model.findOne({
          where: { id },
          paranoid: false,
          include
        });
      }
      res.json(result);
      if (getOptions.log) this.passLog(res, { dados: req.query, registroId: id, tipo: LogType.Read })
    } catch (error) {
      console.error(error);
      handleError(res, { message: 'GET ERROR', error: error.message });
      if (getOptions.logError) this.passLog(res, { error: error.message, registroId: id, status: 'error', tipo: LogType.Read });
    }
    next();
  }

  @Method({ type: 'GET', path: 'history', params: ['id'], defaultMethod: true })
  protected async history(req: Request, res: Response, next: NextFunction) {
    const { id } = req.params;
    const { historyOptions } = this.options;
    try {
      let result;
      const query = queryBuilder.buildQuery(req.query, {
        filter: {
          fields: {
            createdAt: {
              date: true,
            }
          },
          replacements: {
            'usuario.pessoa.nome': 'usuario->pessoa.nome'
          }
        },
        sort: {
          replacements: {
            'usuario.pessoa.nome': 'usuario->pessoa.nome'
          }
        }
      });
      query.where = query.where || {};
      query.where.registroId = id;
      query.where.tipo = { [Op.in]: [LogType.Create, LogType.Update, LogType.Delete, LogType.Restore] };
      const count = await this.logModel.count(query);
      const rows = await this.logModel.findAll(query);
      result = { rows, count };
      res.json(result);
      if (historyOptions.log) this.passLog(res, { dados: req.query, registroId: id, tipo: LogType.History })
    } catch (error) {
      console.error(error);
      handleError(res, { message: 'GET ERROR', error: error.message });
      if (historyOptions.logError) this.passLog(res, { error: error.message, registroId: id, status: 'error', tipo: LogType.History });
    }
    next();
  }


  @Method({ type: 'DELETE', path: false, defaultMethod: true, params: ['id'] })
  protected async delete(req: Request, res: Response, next: NextFunction) {
    const { id } = req.params;
    const { deleteOptions } = this.options;
    try {
      const result = await this.model.destroy({ where: { id } });
      res.json(result);
      if (deleteOptions.log) this.passLog(res, { dados: result, registroId: id, status: 'success', tipo: LogType.Delete });
    } catch (error) {
      handleError(res, { message: 'DELETE ERROR', error: error.message });
      if (deleteOptions.logError) this.passLog(res, { error: error.message, registroId: id, status: 'error', tipo: LogType.Delete });
    }
    next();
  }

  /**Manda as informações de log para middleware responsável. */
  protected passLog(res: Response, log: Partial<Auditoria>) {
    res.locals.log = log;
  }

  /**Midleware para logar informações. */
  private logMiddleware(method: string, options: MethodOptions, req: Request, res: Response, next: NextFunction) {
    const log: Auditoria = res.locals.log;

    //Pegando valores padrão.
    log.metodo = log.metodo || method;
    log.tabela = log.tabela || this.model.tableName;
    log.status = log.status || 'success';
    log.url = log.url || req.headers.referer;
    log.usuarioId = log.usuarioId || (res.locals.user ? res.locals.user.id : null);

    try {
      this.logModel.create(log);
    } catch (error) {
      console.error('LOG ERROR', error);
    }
  }

  /**Registra as rotas para serviço. */
  public register(router?: Router) {
    this.routedMethods = this.getMethodsInfo();
    for (const method in this.routedMethods) {
      const options = this.routedMethods[method];

      //Verificando se um método padrão foi bloqueado.
      if (options.defaultMethod) {
        if (this.options.defaultMethods[method] === false) {
          continue;
        }
      }

      const httpMethodName = options.type.toLowerCase();
      const path = getPathWithParams(this.path, method, options);
      (<any>router)[httpMethodName](path, this[method].bind(this));
      //Inserindo middleware de log.
      if (options.log) {
        (<any>router)[httpMethodName](path, this.logMiddleware.bind(this, method, options))
      }
    }
  }

  /**Busca informações de métodos. */
  private getMethodsInfo() {
    let result: any = {};
    let prototype = Object.getPrototypeOf(this);

    while (prototype && prototype !== BaseService) {
      Object.getOwnPropertyNames(prototype)
        .forEach(name => {
          if (name !== 'constructor') {
            const propDescriptor = Object.getOwnPropertyDescriptor(prototype, name);
            if (propDescriptor && typeof propDescriptor.value === 'function') {
              const methodInfo = getMethodInfo(prototype, name);
              if (methodInfo && !result[name]) {
                result[name] = methodInfo
              }
            }
          }
        })
      prototype = Object.getPrototypeOf(prototype);
    }
    return result;
  }
}