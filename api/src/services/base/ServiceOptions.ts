import { QueryOptions } from "../query/types";
import { IncludeOptions } from "sequelize/types";

/**Configurações de campo. */
export interface FieldConfig {
  /**Substitui nome do campo. */
  replaceWith?: string,
  /**Se o campo é data. */
  date?: boolean,
  /**Configurações de campo de data. */
  dateConfig?: { format: string }
}
export type Fields = { [key: string]: FieldConfig };

export type Replacements = { [key: string]: string };

export type CrudOptions = {
  include?: Array<IncludeOptions>;
}

export interface OperationOptions {
  log?: boolean,
  logError?: boolean
}

export interface GetOptions extends OperationOptions, QueryOptions { }

export interface CreateOptions extends OperationOptions, CrudOptions { }

export interface UpdateOptions extends OperationOptions, CrudOptions { }

export interface DeleteOptions extends OperationOptions { }

export interface RestoreOptions extends OperationOptions { }

export interface HistoryOptions extends OperationOptions { }

/**Opções de um service. */
export default interface ServiceOptions {
  /**Descrição de log. */
  logDescription?: string;
  /**Métodos padrão. */
  defaultMethods?: {
    get?: boolean;
    create?: boolean;
    update?: boolean;
    delete?: boolean;
    restore?: boolean;
    [key: string]: boolean | null;
  },
  /**Opções padrão para métodos de crud. */
  crudOptions?: CrudOptions,

  /**Opções para metodo get. */
  getOptions?: GetOptions
  /**Opções para metodo create. */
  createOptions?: CreateOptions
  /**Opções para metodo update. */
  updateOptions?: UpdateOptions
  /**Opções para metodo delete. */
  deleteOptions?: DeleteOptions
  /**Opções para metodo restore. */
  restoreOptions?: RestoreOptions
  /**Opções para metodo history. */
  historyOptions?: HistoryOptions
}