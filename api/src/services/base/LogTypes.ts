enum LogType {
  Create = 'CREATE',
  Update = 'UPDATE',
  Delete = 'DELETE',
  Restore = 'RESTORE',
  Read = 'READ',
  History = 'HISTORY',
  Login = 'LOGIN',
  Logout = 'LOGOUT'
}

export default LogType;
