import { Model, Transaction, IncludeOptions, Op } from "sequelize";
import sequelize from '../../connection';

type SequelizeModel = (new () => Model<unknown, unknown>) & typeof Model;

export default async function update(model: SequelizeModel, values: any, includes: Array<IncludeOptions>) {
  let transaction;
  try {
    transaction = await sequelize.transaction();

    await updateRecursivaly(model, values, includes, transaction);

    transaction.commit();
  } catch (error) {
    if (transaction) transaction.rollback();
    throw error;
  }
}

export async function updateRecursivaly(model: SequelizeModel, values: any, includes: Array<IncludeOptions>,
  transaction: Transaction, relationKey?: string, parentId?: number) {

  let currentId;

  if (values.id) {
    await model.update(values, { where: { id: values.id }, transaction });
    currentId = values.id;
  } else {
    values[relationKey] = parentId;
    const result: any = await model.create(values, { transaction });
    currentId = result.id;
  }

  for (const key in values) {
    if (isNestedItem(values[key])) {
      const currentInclude = includes.find(item => (item.as && item.as === key) || item.model.name === key);
      if (!currentInclude) throw new Error(`Include para key "${key}" não encontrado.`);
      const associationKey = currentInclude.model.associations[model.name].foreignKey;
      if (!associationKey) throw new Error(`Association key para key "${key}" não encontrado.`);

      if (Array.isArray(values[key])) {
        let idsToNotRemove = [];
        for (const nestedValue of values[key]) {
          const resultId = await updateRecursivaly(currentInclude.model as any, nestedValue,
            currentInclude.include as any, transaction, associationKey, currentId);
          idsToNotRemove.push(resultId);
        }
        await currentInclude.model.destroy({ where: { [associationKey]: currentId, id: { [Op.notIn]: idsToNotRemove } }, transaction });
      } else {
        const resultId = await updateRecursivaly(currentInclude.model as any, values[key],
          currentInclude.include as any, transaction, associationKey, currentId);
        await currentInclude.model.destroy({ where: { [associationKey]: currentId, id: { [Op.ne]: resultId } }, transaction });
      }
    }
  }
  return currentId;
}

function isNestedItem(value: any) {
  return !!value && ((Array.isArray(value) && value.length) || (typeof value === "object" && !(value instanceof Date)))
}

function hasChildren(value: any) {
  return Object.values(value).some(value => isNestedItem(value))
}
