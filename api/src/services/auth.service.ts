import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import config from '../config';
import LogType from './base/LogTypes';
import Method from './servicera/decorators/Method';
import Servicera from './servicera/Servicera';
import UsuariosModel, { Usuario } from '../models/usuarios.model';
import AuditoriaModel, { Auditoria } from '../models/auditorias.model';
import { Request, Response, Router } from 'express';
import { sendConfirmationEmail, sendChangePasswordEmail } from '../helpers/email';

class AuthService extends Servicera<Usuario> {
  constructor() {
    super(null, UsuariosModel, { methods: null });
  }

  @Method({ type: 'POST', path: 'login' })
  async login(req: Request, res: Response, next: any) {
    const { email, password } = req.body;

    const existentUser = await UsuariosModel.findOne({ where: { email } });

    if (existentUser) {
      if (bcrypt.compareSync(password, existentUser.hashSenha)) {
        const { id, email, nome, caminhoImagem, hashConfirmacao } = existentUser;

        if (hashConfirmacao) {
          res.status(401).send('Email não confirmado.');
        }
        const token = jwt.sign({ id, email, nome, caminhoImagem }, config.secretKey);
        res.status(200).json({ token, user: { id, email, nome, caminhoImagem } });
      } else {
        res.status(401).send('Email ou senha não encontrados.');
      }
    } else {
      res.status(401).send('Email ou senha não encontrados.');
    }
  }

  @Method({ type: 'POST', path: 'register' })
  async register(req: Request, res: Response, next: any) {
    try {
      const { nome, email, senha, confirmacaoSenha } = req.body;
      if (senha !== confirmacaoSenha) {
        res.status(400).send('Senha e confirmação não são iguais.');
        return;
      }

      const matchedEmailsCount = await UsuariosModel.count({
        where: { email },
        paranoid: true
      })

      if (matchedEmailsCount && matchedEmailsCount > 0) {
        res.status(400).send('Email ja cadastrado.');
        return;
      }

      const hashConfirmacao = jwt.sign({ email }, config.secretKey, { expiresIn: '1d' });

      await sendConfirmationEmail(email, hashConfirmacao);

      const hashSenha = bcrypt.hashSync(senha, bcrypt.genSaltSync());

      await UsuariosModel.create({ nome, email, hashSenha, hashConfirmacao });

      res.status(200).send('Usuário criado com sucesso.');
    } catch (error) {
      console.error(error);
      res.status(500).json('Erro ao registrar usuário.');
    }
  }

  @Method({ type: 'POST', path: 'confirm' })
  async confirm(req: Request, res: Response, next: any) {
    try {
      const { hash } = req.body;

      const { email } = jwt.verify(hash, config.secretKey) as any;

      const existentUser = await UsuariosModel.findOne({ where: { email } });

      if (existentUser == null) {
        res.status(400).send('Usuário não encontrado.');
        return;
      }

      if (existentUser.hashConfirmacao == null) {
        res.status(400).send('Usuário ja confirmado.');
        return;
      }

      await existentUser.update({ hashConfirmacao: null });

      const { id, nome, caminhoImagem } = existentUser;
      const token = jwt.sign({ id, email, nome, caminhoImagem }, config.secretKey);
      res.status(200).json({ token });

    } catch (error) {
      res.send(400).send('Erro ao confirmar cadastro.');
    }
  }

  @Method({ type: 'POST', path: 'changePasswordRequest' })
  async changePasswordRequest(req: Request, res: Response, next: any) {
    try {
      const { email } = req.body;

      const existentUser = await UsuariosModel.findOne({ where: { email }, paranoid: true });

      if (existentUser == null) {
        res.status(400).send('Usuário não encontrado.');
        return;
      }

      const { id } = existentUser;

      const hash = jwt.sign({ id }, config.secretKey, { expiresIn: '1d' });

      await sendChangePasswordEmail(email, hash);

      res.send('Email de troca de senha enviado.');
      return;
    } catch (error) {
      res.status(400).send('Erro ao requisitar troca de senha.');
      return;
    }
  }

  @Method({ type: 'POST', path: 'changePassword' })
  async changePassword(req: Request, res: Response, next: any) {
    try {
      const { hash, senha, confirmacaoSenha } = req.body;
      if (senha !== confirmacaoSenha) {
        res.status(400).send('Senha e confirmação não são iguais.');
        return;
      }

      const { id } = jwt.verify(hash, config.secretKey) as any;

      const hashSenha = bcrypt.hashSync(senha, bcrypt.genSaltSync());

      await UsuariosModel.update({ hashSenha }, { where: { id } });

      res.status(200).send('Senha atualizada com sucesso.');
    } catch (error) {
      console.error(error);
      res.status(500).json('Erro ao atualizar senha de usuário.');
    }
  }

  async getUser(req: Request, res: Response, next: any) {
    const { id } = res.locals.user;
    try {
      const result = await UsuariosModel.findOne({ where: { id } });
      const { id: userId, nome, email, caminhoImagem } = result;

      res.json({ id: userId, nome, email, caminhoImagem });

      const log: Auditoria = {
        metodo: 'getUser',
        tabela: UsuariosModel.tableName,
        tipo: LogType.Login,
        url: req.headers.referer,
        usuarioId: id,
        registroId: id
      }
      AuditoriaModel.create(log)
    } catch (error) {

      res.status(500).json({ error: 'Usuário não encontrado.' })

      const log: Auditoria = {
        metodo: 'getUser',
        status: 'error',
        error: error.message,
        tabela: UsuariosModel.tableName,
        tipo: LogType.Login,
        url: req.headers.referer,
        usuarioId: id,
        registroId: id
      }

      AuditoriaModel.create(log)
    }
  }

  async logout(req: Request, res: Response, next: any) {
    res.json({ ok: true });
  }

  authenticate(req: Request, res: Response, next: any) {
    if (req.method === 'OPTIONS') {
      return next();
    }
    const token = req.body.token || req.query.token || req.headers['authorization'];
    if (!token) {
      return res.status(403).send('Token não informado.');
    }
    try {
      const verifyResult = jwt.verify(token, config.secretKey);
      res.locals.user = verifyResult;
      next();
    } catch (error) {
      res.status(403).send('Token inválido.');
    }
  }

  public initializeProtectedRoutes(protectedRoute: Router) {
    protectedRoute.use(this.authenticate.bind(this));
    protectedRoute.get('/getUser', this.getUser.bind(this));
    protectedRoute.post('/logout', this.logout.bind(this));
  }
}

export default new AuthService();
