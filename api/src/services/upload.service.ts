import { Request, Response } from 'express';
import multer from 'multer';
import fs from 'fs';
import path from 'path';

const appDir = path.dirname(require.main.filename);

const directory = appDir + '/' + '/uploads';

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    if (!fs.existsSync(directory)) {
      fs.mkdirSync(directory);
    }
    cb(null, directory);
  },
  filename: function (req, file, cb) {
    cb(null, file.fieldname + '-' + Date.now() + '.' + file.mimetype.split('/')[1])
  }
})

const upload = multer({ storage })

export default (app: any) => {
  app.post('/upload', upload.single('file'), function (req: Request, res: Response, next: any) {
    res.status(200).json({ filename: req.file.filename });
  })
}

