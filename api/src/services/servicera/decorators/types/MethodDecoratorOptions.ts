/**Method decorator options */
export default interface MethodDecoratorOptions {
  type?: 'USE' | 'GET' | 'POST' | 'PUT' | 'PATCH' | 'DELETE';
  path?: string | Boolean;
  log?: boolean;
  params?: string[];
  _default?: boolean
}