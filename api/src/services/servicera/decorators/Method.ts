import 'reflect-metadata';
import MethodDecoratorOptions from "./types/MethodDecoratorOptions";

/**Key to indetify decorator */
export const METHOD_KEY = Symbol('method');

/**Default decorator options. */
export const DEFAULT_OPTIONS: MethodDecoratorOptions = {
  type: 'USE',
  path: undefined,
  params: undefined,
  log: true,
}

/**Return info about informed target. */
export function getMethodInfo(target: any, key: string) {
  const options = Reflect.getMetadata(METHOD_KEY, target, key);
  return options ? { ...DEFAULT_OPTIONS, ...options } : null;
}

/**Say if the function is a express method. */
export default function Method(options: MethodDecoratorOptions) {
  return Reflect.metadata(METHOD_KEY, options);
}

