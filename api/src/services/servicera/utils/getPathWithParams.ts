/**Returns the path of sequelize method with params. */
export default function getPathWithParams(initial: string, path: string | Boolean, params: string[]) {
  let result;

  result = initial || '';

  if (path && typeof path === "string") {
    result = result.concat('/', path);
  }

  if (params) {
    result = result.concat('/', params.map(item => ':'.concat(item)).join('/'));
  }

  return result;
}