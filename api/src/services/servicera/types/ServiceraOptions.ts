export type ServiceraMethods = 'create' | 'update' | 'delete' | 'get' | 'getById';

export default interface ServiceraOptions {
  methods: Array<ServiceraMethods>,
  debug: boolean,
}