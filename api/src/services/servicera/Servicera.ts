import ServiceraOptions from "./types/ServiceraOptions";
import MethodDecoratorOptions from "./decorators/types/MethodDecoratorOptions";
import getPathWithParams from "./utils/getPathWithParams";
import Method, { getMethodInfo } from "./decorators/Method";
import { Model } from "sequelize";
import { Router, Request, Response } from "express";

type SequelizeModel = (new () => Model<unknown, unknown>) & typeof Model;

type MethodsOptions = { [methodName: string]: MethodDecoratorOptions };

const DEFAULT_OPTIONS: ServiceraOptions = {
  methods: ['create', 'update', 'delete', 'get', 'getById'],
  debug: false
}

/**Reusable service class. */
export default class Servicera<Entity> {
  /**Global config shared with all instances. */
  private static globalOptions: ServiceraOptions = DEFAULT_OPTIONS;
  /**Method to set the global options. */
  public static setGlobalOptions(globalOptions: Partial<ServiceraOptions> = DEFAULT_OPTIONS) {
    Servicera.globalOptions = { ...DEFAULT_OPTIONS, ...globalOptions };
  }

  /**Servicera options. */
  private options: ServiceraOptions = undefined;
  /**Path to found service in express. */
  private path: string = undefined;
  /**Sequelize model. */
  private model: SequelizeModel = undefined;
  /**Object with routed method options by method name. */
  private methodsOptions: MethodsOptions = {};

  constructor(path: string, model: SequelizeModel, options?: Partial<ServiceraOptions>) {
    this.path = path;
    this.model = model;
    this.options = { ...Servicera.globalOptions, ...options };

    this.registerMethods.bind(this);
  }

  //#region REGISTRATION METHODS

  @Method({ type: 'POST', path: false, _default: true })
  /**Create method. */
  public async create(req: Request, res: Response, next: any) {
    try {
      const { body } = req;
      const result = await this.model.create(body);
      res.json(result);
    } catch (error) {
      console.error(error);
      res.status(500).json(error);
    }
  }

  @Method({ type: 'PUT', path: false, params: ['id'], _default: true })
  /**Update method. */
  public async update(req: Request, res: Response, next: any) {
    try {
      const { body, query: { id } } = req;
      const result = await this.model.update(body, { where: { id } });;
      res.json(result);
    } catch (error) {
      console.error(error);
      res.status(500).json(error);
    }
  }

  @Method({ type: 'DELETE', path: false, params: ['id'], _default: true })
  /**Delete method. */
  public async delete(req: Request, res: Response, next: any) {
    try {
      const { query: { id } } = req;
      const result = await this.model.destroy({ where: { id } });
      res.json(result);
    } catch (error) {
      console.error(error);
      res.status(500).json(error);
    }
  }

  @Method({ type: 'GET', path: false, _default: true })
  /**Get method. */
  public async get(req: Request, res: Response, next: any) {
    try {
      const { query: { pageSize, first } } = req;
      const result = await this.model.findAll();
      res.json(result);
    } catch (error) {
      console.error(error);
      res.status(500).json(error);
    }
  }

  @Method({ type: 'GET', path: false, params: ['id'], _default: true })
  /**Get by id method. */
  public async getById(req: Request, res: Response, next: any) {
    try {
      const { query: { id } } = req;
      const result = await this.model.findByPk(id);
      res.json(result);
    } catch (error) {
      console.error(error);
      res.status(500).json(error);
    }
  }

  /**Wrapper for all methods. */
  public async wrapper(method: string, currentOptions: MethodDecoratorOptions, req: Request, res: Response, next: any) {
    const currentFunction = (this as any)[method](req, res, next);

    if (currentFunction && typeof currentFunction.then === 'function') {
      await currentFunction;
    }
  }

  //#endregion

  //#region EXPRESS REGISTRATION

  /**Register marked methods in express. */
  public registerMethods(router: Router) {
    const { debug, methods: allowedMethods } = this.options;

    if (debug) {
      console.log(`REGISTRANDO SERVICE "${this.path}"`)
    }

    this.methodsOptions = this.getMethodsInfo();

    for (const method in this.methodsOptions) {
      const currentOptions = this.methodsOptions[method];

      //Verificando se os metodos devem ser inseridos.
      if (currentOptions._default && (!allowedMethods || allowedMethods.indexOf(method as any) === -1)) {
        continue;
      }

      const { type, path, params } = currentOptions;

      const httpMethodName = type.toLowerCase();

      const pathResult = getPathWithParams(this.path, path != null ? path : method, params);

      if (this.options.debug) {
        console.log(`Registering method "${method}" on path "${pathResult}".`);
      }

      (<any>router)[httpMethodName](pathResult, this.wrapper.bind(this, method, currentOptions))
    }
  }

  /**Search for marked methods in each prototype. */
  private getMethodsInfo() {
    let result: any = {};
    let prototype = Object.getPrototypeOf(this);

    while (prototype && prototype !== Servicera) {
      Object.getOwnPropertyNames(prototype)
        .forEach(name => {
          if (name !== 'constructor') {
            const propDescriptor = Object.getOwnPropertyDescriptor(prototype, name);
            if (propDescriptor && typeof propDescriptor.value === 'function') {
              const methodInfo = getMethodInfo(prototype, name);
              if (methodInfo && !result[name]) {
                result[name] = methodInfo
              }
            }
          }
        })
      prototype = Object.getPrototypeOf(prototype);
    }
    return result;
  }

  //#endregion
}