import { Op, where, col, fn, cast } from "sequelize";
import { FilterOptions } from "./types";
import { getFieldName } from "./utils";

const DATE_FORMAT = 'DD/MM/YYYY';

export enum Operators {
  like = 'lk',
  gt = 'gt',
  lt = 'lt',
  gte = 'gte',
  lte = 'lte'
}

export function getOperatorWhere(fieldWithOperator: string, value: string, options: FilterOptions): any {
  const [field, operator] = fieldWithOperator.split('$');
  const fieldName = getFieldName(field, options);
  const fieldOption = options && options.fields && options.fields[field];

  let column;

  if (fieldOption) {
    if (fieldOption.date) {
      column = fn('to_char', col(fieldName), DATE_FORMAT);
    } else {
      column = cast(col(fieldName), 'text');
    }
  } else {
    column = cast(col(fieldName), 'text');
  }

  switch (operator) {
    case Operators.like:
      return where(column, Op.like as any, `%${value}%`);
    case Operators.gt:
      return where(column, Op.gt as any, value);
    case Operators.gte:
      return where(column, Op.gte as any, value);
    case Operators.lt:
      return where(column, Op.lt as any, value);
    case Operators.lte:
      return where(column, Op.lte as any, value);
    default:
      return where(column, Op.eq as any, value);
  }
}