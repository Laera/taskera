import * as defaulValues from './default';
import { SortOptions, FilterOptions, QueryOptions } from "./types";
import { canUseField, getFieldName, isQueryParameter, getModel, hasFilter, hasWhere, hasIncludedIds } from "./utils";
import { getOperatorWhere } from "./operators";
import { Op, col } from "sequelize";
import { merge } from 'lodash';

export enum Identifiers {
  Sort = '$sort',
  Skip = '$skip',
  Limit = '$limit',
  Order = '$order',
  Include = '$include',
  IncludedIds = '$includedIds',
  Status = '$status'
}

const VALUE_SEPARATOR = ',';
const ORDER_SEPARATOR = '|';

export function doSkip(skip: number) {
  if (skip) {
    const result = Number(skip);
    return result;
  }
}

export function doLimit(limit: number) {
  if (limit) {
    const result = Number(limit);
    return result;
  }
}

export function doSort(sort: string, options?: SortOptions): Array<any> {
  if (sort) {
    let result: Array<any> = [];
    const sortConjuncts = sort.split(VALUE_SEPARATOR);
    for (const sortItem of sortConjuncts) {
      const [field, orientation] = sortItem.split(ORDER_SEPARATOR);
      if (canUseField(field, options)) {
        result.push([col(getFieldName(field, options)), orientation]);
      }
    }
    return result;
  }
}

export function doFilter(filter: any, options?: FilterOptions) {
  let result: any = {}
  for (const field in filter) {
    if (filter[field] && !isQueryParameter(field) && canUseField(field, options)) {
      result[field] = getOperatorWhere(field, filter[field], options);
    }
  }
  return result;
}

export function doId(id: string) {
  if (id) {
    return id
  }
}

export function doIncludedIds(ids: string) {
  if (ids) {
    return ids.split(VALUE_SEPARATOR);
  }
}

export function doIncludeRecursivaly(include: string, result: any = {}) {
  if (include) {
    result.include = []
    for (const includeKey of include.split(VALUE_SEPARATOR)) {
      const allIncludes = includeKey.split('.');
      const currentInclude: any = {};
      currentInclude.model = getModel(allIncludes[0]);
      if (allIncludes.length > 1) {
        doIncludeRecursivaly(allIncludes.slice(1).join('.'), currentInclude);
      }
      result.include.push(currentInclude);
    }
  }
  return result;
}

export function doInclude(include: string) {
  if (include) {
    const result: any = doIncludeRecursivaly(include);
    return result.include;
  }
}

export function doStatus(status: string): any {
  if (status === 'enabled') {
    return { [Op.eq]: null }
  }
  else if (status === 'disabled') {
    return { [Op.ne]: null }
  } else {
    return {
      [Op.or]: {
        [Op.ne]: null,
        [Op.eq]: null
      }
    }
  }
}

export function buildQuery(query: any, queryOptions: QueryOptions): any {
  const options: QueryOptions = merge(defaulValues.defaultQuery, queryOptions);
  let result: any = {};
  result.raw = true;
  result.subQuery = false;
  result.paranoid = false;

  if (hasWhere(query)) {
    result.where = { [Op.or]: { [Op.and]: {} } };

    if (hasFilter(query)) {
      result.where[Op.or][Op.and][Op.or] = doFilter(query, queryOptions.filter);
    }

    if (options.statusCheck) {
      result.where[Op.or][Op.and].deletedAt = doStatus(query.$status);
    }

    if (hasIncludedIds(query.$includedIds)) {
      result.where[Op.or].id = {
        [Op.in]: doIncludedIds(query.$includedIds)
      }
    }
  } else if (options.statusCheck) {
    result.where = { deletedAt: doStatus(query.$status) };
  }

  result.include = doInclude(query.$include);
  result.offset = doSkip(query.$skip);
  result.limit = doLimit(query.$limit);
  result.order = doSort(query.$sort, options.sort);
  return result;
}