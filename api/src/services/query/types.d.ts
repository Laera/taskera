export interface IncludeOptions { }

export interface SortOptions extends FieldManager { }

export interface FieldManager {
  include?: Array<string>,
  exclude?: Array<string>,
  replacements?: any
}

export interface FilterFieldOption {
  date?: boolean,
  format?: any
}

export interface FilterOptions extends FieldManager {
  fields?: { [key: string]: FilterFieldOption },
}

/**Opções da tabela. */
export interface QueryOptions {
  /**Indica se deve ser feita a checagem de status. */
  statusCheck?: boolean,
  /**Configurações de filtragem. */
  filter?: FilterOptions,
  /**Configurações de ordenação. */
  sort?: SortOptions,
  /**Includes que podem ser feitos. */
  include?: IncludeOptions,
  /**Substituições de nomes. */
  replacements?: { [key: string]: string }
}