import { QueryOptions, FieldManager } from "./types";
import { Identifiers } from "./query";
import { Model } from "sequelize";

export function canUseField(field: string, options: FieldManager): boolean {
  if (options) {
    if (options.include && options.include.length) {
      return options.include.some(item => item === field);
    } else if (options.exclude && options.exclude.length) {
      return !options.exclude.some(item => item === field);
    }
  }
  return true;
}

export function getFieldName(field: string, options: QueryOptions): string {
  if (options) {
    const { replacements } = options;
    for (const key in replacements) {
      const regex = RegExp(key.replace(/\./g, '\.'));
      if (regex.test(key)) {
        field = field.replace(regex, replacements[key]);
      }
    }

  }
  return field.split('.').map(item => `"${item}"`).join('.');
}

export function isQueryParameter(field: string): boolean {
  if (Object.values(Identifiers).includes(field as any)) {
    return true;
  }
  return false;
}

export function hasFilter(query: any): boolean {
  return Object.keys(query).some((key: string) => !isQueryParameter(key) && query[key]);
}

export function hasIncludedIds(includedIds: string) {
  return !!includedIds;
}

export function hasWhere(query: any) {
  return hasFilter(query) || hasIncludedIds(query.$includedIds);
}

export function getModel(name: string): typeof Model {
  try {
    const model = require(`../../models/${name}.model`).default;
    return model;
  } catch (error) {
    console.error('Erro ao importar model: ', error.message)
  }
}
