import Servicera from "./servicera/Servicera";
import UsuariosModel, { Usuario } from "../models/usuarios.model";

class UsuarioService extends Servicera<Usuario> {
  constructor(){
    super('usuarios', UsuariosModel);
  }
}

export default new UsuarioService();