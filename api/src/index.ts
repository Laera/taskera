import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import config from './config';
import http from 'http';
import Socket from './socket';
import registerUploadRoute from './services/upload.service';
import Servicera from './services/servicera/Servicera';
//Service imports
import authService from './services/auth.service';
import usuarioService from './services/usuario.service';
//Inicializando models.
import './models';

//Inicializando servicera.
Servicera.setGlobalOptions({
  debug: true
})

const app = express();
const httpServer = http.createServer(app);

Socket.initialize(httpServer);

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//API aberta
const openApi = express.Router();
authService.registerMethods(openApi);
app.use(config.openApiPrefix, openApi);
//API protegida
const protectedApi = express.Router();
//Inicializando rota protegida.
authService.initializeProtectedRoutes(protectedApi);
//Registrando serviços.
usuarioService.registerMethods(protectedApi);
app.use(config.protectedApiPrefix, protectedApi);
//Uploads.
protectedApi.use('/uploads', express.static(__dirname + '/uploads'));
registerUploadRoute(protectedApi);

httpServer.listen(config.port, function () {
  console.log(`BACKEND INICIALIZADO EM AMBIENTE ${config.description} NA PORTA ${config.port}`);
})
