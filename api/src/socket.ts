import WebSocket from 'ws';
import jwt from 'jsonwebtoken';
import config from './config';
import { Server } from 'http';

/**Autentica acesso via socket. */
function authenticate(info: any, cb: any) {
  try {
    const token = info.req.headers['sec-websocket-protocol'];
    const result = jwt.verify(token, config.secretKey);
    info.req.user = result;
    cb(true);
  } catch (error) {
    cb(false, 401, 'Unauthorized')
  }
}

export default class Socket {
  private static socket: WebSocket.Server;
  private static clients: { [key: string]: Array<WebSocket> } = {};
  private static listeners: { [key: string]: Array<Function> } = { message: [], default: [] };

  /**Adiciona função para escutar evento de mensagem. */
  public static addListener(on: 'message' | 'default', callback: Function) {
    Socket.listeners[on].push(callback);
  }

  /**Retorna se usuario esta online por id. */
  public static isUserOnline(id: string): Boolean {
    return !!(Socket.clients[id] && Socket.clients[id].length);
  }

  /**Envia mensagens via socket. */
  public static send(type: 'clientsChange' | 'notification' | 'message' | 'default', data: any = {}, clientIds?: string | Array<string>) {
    if (clientIds) {
      if (Array.isArray(clientIds)) {
        clientIds.forEach(clientId => Socket.sendByClient(clientId, type, data))
      } else {
        Socket.sendByClient(clientIds, type, data);
      }
    } else {
      for (const key in Socket.clients) {
        this.sendByClient(key, type, data);
      }
    }
  }

  /**Envia mensagens por cliente. */
  private static sendByClient(clientId: string, type: string, data: any) {
    if (Socket.clients[clientId] && Socket.clients[clientId].length) {
      const message = { type, data };
      const stringMessage = JSON.stringify(message);
      for (const socket of Socket.clients[clientId]) {
        socket.send(stringMessage);
      }
    }
  }

  /**Adiciona cliente. */
  private static addClient(userId: string, client: WebSocket) {
    if (Socket.clients[userId]) {
      Socket.clients[userId].push(client);
    } else {
      Socket.clients[userId] = [client];
    }
  }

  /**Remove client. */
  private static removeClient(userId: string, client: WebSocket) {
    if (Socket.clients[userId]) {
      const indexOf = Socket.clients[userId].indexOf(client);
      if (indexOf > -1) {
        Socket.clients[userId].splice(indexOf, 1);
      }
    }
  }

  /**Inicializa socket. */
  public static initialize(server: Server) {
    Socket.socket = new WebSocket.Server({ server, verifyClient: authenticate });;

    Socket.socket.on('connection', (clientSocket: WebSocket, info: any) => {
      Socket.addClient(info.user._id, clientSocket);
      Socket.send('clientsChange');

      clientSocket.on('close', () => {
        Socket.removeClient(info.user._id, clientSocket);
        Socket.send('clientsChange');
      });

      clientSocket.on('message', (data) => {
        const message = JSON.parse(data.toString());
        switch (message.type) {
          case 'message':
            Socket.listeners.message.forEach(cb => cb(message.data, info))
            break;
          default:
            Socket.listeners.default.forEach(cb => cb(message.data, info))
        }
      })
    });
  }
}

