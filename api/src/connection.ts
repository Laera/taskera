import { Sequelize } from 'sequelize';

const connection = new Sequelize({
  host: 'www.laerasoftware.com',
  dialect: 'postgres',
  database: 'taskera',
  username: 'remote',
  password: 'Q27l3p98',
  logging: false,
  define: {
    createdAt: 'criadoEm',
    updatedAt: 'atualizadoEm',
    deletedAt: 'removidoEm',
    paranoid: true,
    timestamps: true,
    freezeTableName: true,
  }
});

connection.authenticate()
  .then(() => console.log('Conexão com o banco bem sucedida.'))
  .catch((error: any) => console.log('Erro ao conectar.', error))

export default connection;