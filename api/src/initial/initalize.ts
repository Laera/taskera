import initUsuarios from "./usuarios";

export default async function initializeData() {
  await initUsuarios();
}