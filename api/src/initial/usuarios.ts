import UsuariosModel, { Usuario } from '../models/usuarios.model';

const usuarios: Usuario[] = [
  {
    nome: 'Felipe Laera Ignacio',
    email: 'laera.felipe@gmail.com',
    dataNascimento: new Date('1996-10-10'),
    hashConfirmacao: null,
    hashSenha: '$2b$10$0/zWsD1R3k2jdVAdMgWLhuR0Guo53DEs0OgeYxRaivxphQiPuejYu',
    criadoEm: new Date(),
  }
]

export default async function initUsuarios() {
  await UsuariosModel.bulkCreate(usuarios);
}

