import sequelize from '../connection';
import { Model, DataTypes } from 'sequelize';

export interface Projeto {
  id?: number,
  usuarioId?: number,
  descricao: string,
  objetivo: string,
  plano: number,
  caminhoImagem?: string,
  criadoEm?: Date,
  atualizadoEm?: Date,
  removidoEm?: Date,
}

class ProjetosModel extends Model implements Projeto {
  id?: number;
  usuarioId?: number;
  descricao: string;
  objetivo: string;
  plano: number;
  caminhoImagem?: string;
  criadoEm?: Date;
  atualizadoEm?: Date;
  removidoEm?: Date;

  public static associate() {
    // ProjetosModel.belongsTo(UsuariosModel, { foreignKey: 'usuarioId' });
  }
}

ProjetosModel.init(
  {
    usuarioId: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false
    },
    descricao: {
      type: DataTypes.STRING(500),
      allowNull: false
    },
    objetivo: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    plano: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    caminhoImagem: {
      type: DataTypes.STRING(500),
      allowNull: true
    }
  },
  {
    sequelize,
    timestamps: true,
    tableName: 'projetos',
    modelName: 'projetos'
  }
);

export default ProjetosModel;

