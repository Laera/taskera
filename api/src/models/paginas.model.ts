import sequelize from '../connection';
import PermissoesModel from './permissoes.model';
import PaginasPermissoesModel from './paginas_permissoes.model';
import { Model, DataTypes } from 'sequelize';

export interface Pagina {
  id?: number,
  caminho: string,
  descricao: string,
}

class PaginasModel extends Model implements Pagina {
  id?: number;
  caminho: string;
  descricao: string;

  static associate() {
    this.belongsToMany(PermissoesModel, { through: PaginasPermissoesModel })
  }
}

PaginasModel.init(
  {
    caminho: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    descricao: {
      type: DataTypes.STRING(255),
      allowNull: false
    }
  },
  {
    sequelize,
    timestamps: false,
    tableName: 'paginas',
    modelName: 'paginas'
  }
);

export default PaginasModel;

