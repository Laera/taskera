import sequelize from '../connection';
import { Model, DataTypes } from "sequelize";

export interface Auditoria {
  id?: number;
  url: string;
  tipo: string;
  tabela?: string;
  metodo?: string;
  usuarioId?: number;
  registroId?: number;
  dados?: any;
  status?: string;
  error?: string;
  createdAt?: Date;
}

class AuditoriaModel extends Model<Auditoria> implements Auditoria {
  id?: number;
  url: string;
  tipo: string;
  tabela?: string;
  metodo?: string;
  usuarioId?: number;
  registroId?: number;
  status?: string;
  dados?: any;
  error?: string;
  createdAt?: Date;

  // public static associate() {
  //   AuditoriaModel.belongsTo(UsuariosModel, { foreignKey: 'usuarioId' })
  // }
}

AuditoriaModel.init({
  url: {
    type: DataTypes.STRING(500),
    allowNull: false
  },
  tipo: {
    type: DataTypes.STRING(10),
    allowNull: false
  },
  tabela: {
    type: DataTypes.STRING(200),
    allowNull: false
  },
  status: {
    type: DataTypes.STRING(10),
    allowNull: true
  },
  metodo: {
    type: DataTypes.STRING(50),
    allowNull: true
  },
  dados: {
    type: DataTypes.JSON
  },
  registroId: {
    type: DataTypes.INTEGER
  }
},
  {
    sequelize,
    timestamps: true,
    updatedAt: false,
    tableName: 'auditorias',
    modelName: 'auditorias'
  }
)

export default AuditoriaModel;