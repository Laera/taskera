import sequelize from '../connection';
import PaginasModel from './paginas.model';
import PaginasPermissoesModel from './paginas_permissoes.model';
import { Model, DataTypes } from 'sequelize';

export interface Permissoes {
  id?: number,
  caminho: string,
  descricao: string,
}

class PermissoesModel extends Model implements Permissoes {
  id?: number;
  caminho: string;
  descricao: string;

  static associate() {
    this.belongsToMany(PaginasModel, { through: PaginasPermissoesModel })
  }
}

PermissoesModel.init(
  {
    descricao: {
      type: DataTypes.STRING(255),
      allowNull: false
    }
  },
  {
    sequelize,
    timestamps: false,
    tableName: 'permissoes',
    modelName: 'permissoes'
  }
);

export default PermissoesModel;

