import sequelize from '../connection';
import { Model, DataTypes} from 'sequelize';

export interface Usuario {
  id?: number,
  nome: string,
  email: string,
  hashSenha: string,
  hashConfirmacao: string,
  caminhoImagem?: string,
  confirmado?: string,
  dataNascimento?: Date,
  criadoEm?: Date,
  atualizadoEm?: Date,
  removidoEm?: Date,
}

class UsuariosModel extends Model implements Usuario {
  id?: number;
  nome: string;
  email: string;
  hashSenha: string;
  hashConfirmacao: string;
  caminhoImagem?: string;
  dataNascimento?: Date;
  criadoEm?: Date;
  atualizadoEm?: Date;
  removidoEm?: Date;

  public static associate() {
    // UsuariosModel.hasMany(ProjetosModel, { foreignKey: 'usuarioId' });
    // UsuariosModel.hasMany(AuditoriaModel, { foreignKey: 'usuarioId' });
  }
}

UsuariosModel.init(
  {
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      autoIncrement: true,
      primaryKey: true,
    },
    nome: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    email: {
      type: DataTypes.STRING(500),
      allowNull: false
    },
    caminhoImagem: {
      type: DataTypes.STRING(500),
      allowNull: true
    },
    dataNascimento: {
      type: DataTypes.DATE,
      allowNull: true
    },
    hashConfirmacao: {
      type: DataTypes.STRING(255),
      allowNull: true,
    },
    hashSenha: {
      type: DataTypes.STRING(255),
      allowNull: false,
    }
  },
  {
    sequelize,
    timestamps: true,
    tableName: 'usuarios',
    modelName: 'usuarios'
  }
);

export default UsuariosModel;

