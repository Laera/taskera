import connection from '../connection';
import UsuariosModel from "./usuarios.model";
import ProjetosModel from "./projetos.model";
import AuditoriaModel from "./auditorias.model";
import PaginasModel from './paginas.model';
import PermissoesModel from './permissoes.model';
import PaginasPermissoesModel from './paginas_permissoes.model';

PermissoesModel.associate();
PaginasModel.associate();
PaginasPermissoesModel.associate();

import initializeData from '../initial/initalize';

connection.sync({ force: true, logging: sql => console.log('sql', sql) })
  .then(() => console.log('BANCO DE DADOS SINCRONIZADO'))
  .catch(error => console.log('error', error));


export {
  UsuariosModel,
  ProjetosModel,
  AuditoriaModel,
  PaginasModel,
  PermissoesModel
};
