import sequelize from '../connection';
import PermissoesModel from './permissoes.model';
import PaginasModel from './paginas.model';
import { Model, DataTypes } from 'sequelize';

export interface PaginasPermissoes {
  id?: number,
  paginaId: number,
  permissaoId: number,
}

class PaginasPermissoesModel extends Model implements PaginasPermissoes {
  id?: number;
  paginaId: number;
  permissaoId: number;

  static associate() {
    this.belongsTo(PaginasModel);
    this.belongsTo(PermissoesModel);
  }
}

PaginasPermissoesModel.init(
  {
    paginaId: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false
    },
    permissaoId: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false
    }
  },
  {
    sequelize,
    timestamps: false,
    tableName: 'paginas_permissoes',
    modelName: 'paginas_permissoes'
  }
);

export default PaginasPermissoesModel;

